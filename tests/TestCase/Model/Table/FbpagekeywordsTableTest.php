<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FbpagekeywordsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FbpagekeywordsTable Test Case
 */
class FbpagekeywordsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FbpagekeywordsTable
     */
    protected $Fbpagekeywords;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Fbpagekeywords',
        'app.Fbpages',
        'app.Keywords',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Fbpagekeywords') ? [] : ['className' => FbpagekeywordsTable::class];
        $this->Fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Fbpagekeywords);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FbpagekeywordsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FbpagekeywordsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
