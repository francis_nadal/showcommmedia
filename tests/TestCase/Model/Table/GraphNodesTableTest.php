<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GraphNodesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GraphNodesTable Test Case
 */
class GraphNodesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GraphNodesTable
     */
    protected $GraphNodes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.GraphNodes',
        'app.Articles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('GraphNodes') ? [] : ['className' => GraphNodesTable::class];
        $this->GraphNodes = $this->getTableLocator()->get('GraphNodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GraphNodes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\GraphNodesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\GraphNodesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
