<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UnusedarticlesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UnusedarticlesTable Test Case
 */
class UnusedarticlesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UnusedarticlesTable
     */
    protected $Unusedarticles;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Unusedarticles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Unusedarticles') ? [] : ['className' => UnusedarticlesTable::class];
        $this->Unusedarticles = $this->getTableLocator()->get('Unusedarticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Unusedarticles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\UnusedarticlesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
