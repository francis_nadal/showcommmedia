<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArticleprocessqueuesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArticleprocessqueuesTable Test Case
 */
class ArticleprocessqueuesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArticleprocessqueuesTable
     */
    protected $Articleprocessqueues;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Articleprocessqueues',
        'app.Articles',
        'app.Fbpages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Articleprocessqueues') ? [] : ['className' => ArticleprocessqueuesTable::class];
        $this->Articleprocessqueues = $this->getTableLocator()->get('Articleprocessqueues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Articleprocessqueues);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ArticleprocessqueuesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ArticleprocessqueuesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
