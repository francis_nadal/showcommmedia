<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RawarticlesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RawarticlesTable Test Case
 */
class RawarticlesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RawarticlesTable
     */
    protected $Rawarticles;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Rawarticles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Rawarticles') ? [] : ['className' => RawarticlesTable::class];
        $this->Rawarticles = $this->getTableLocator()->get('Rawarticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Rawarticles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\RawarticlesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
