<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FbpagesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FbpagesTable Test Case
 */
class FbpagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FbpagesTable
     */
    protected $Fbpages;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Fbpages',
        'app.Articleprocessqueues',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Fbpages') ? [] : ['className' => FbpagesTable::class];
        $this->Fbpages = $this->getTableLocator()->get('Fbpages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Fbpages);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FbpagesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
