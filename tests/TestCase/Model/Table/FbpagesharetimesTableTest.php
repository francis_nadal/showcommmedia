<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FbpagesharetimesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FbpagesharetimesTable Test Case
 */
class FbpagesharetimesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FbpagesharetimesTable
     */
    protected $Fbpagesharetimes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Fbpagesharetimes',
        'app.Fbpages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Fbpagesharetimes') ? [] : ['className' => FbpagesharetimesTable::class];
        $this->Fbpagesharetimes = $this->getTableLocator()->get('Fbpagesharetimes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Fbpagesharetimes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FbpagesharetimesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FbpagesharetimesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
