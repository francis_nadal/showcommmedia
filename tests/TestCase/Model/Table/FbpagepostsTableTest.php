<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FbpagepostsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FbpagepostsTable Test Case
 */
class FbpagepostsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FbpagepostsTable
     */
    protected $Fbpageposts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Fbpageposts',
        'app.Fbpages',
        'app.Keywords',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Fbpageposts') ? [] : ['className' => FbpagepostsTable::class];
        $this->Fbpageposts = $this->getTableLocator()->get('Fbpageposts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Fbpageposts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FbpagepostsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FbpagepostsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
