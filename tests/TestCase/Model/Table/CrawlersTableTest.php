<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CrawlersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CrawlersTable Test Case
 */
class CrawlersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CrawlersTable
     */
    protected $Crawlers;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Crawlers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Crawlers') ? [] : ['className' => CrawlersTable::class];
        $this->Crawlers = $this->getTableLocator()->get('Crawlers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Crawlers);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CrawlersTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
