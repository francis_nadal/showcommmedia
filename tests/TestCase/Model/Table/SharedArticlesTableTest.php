<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SharedArticlesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SharedArticlesTable Test Case
 */
class SharedArticlesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SharedArticlesTable
     */
    protected $SharedArticles;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SharedArticles',
        'app.Articles',
        'app.Fbpages',
        'app.GraphNodes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SharedArticles') ? [] : ['className' => SharedArticlesTable::class];
        $this->SharedArticles = $this->getTableLocator()->get('SharedArticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SharedArticles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SharedArticlesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SharedArticlesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
