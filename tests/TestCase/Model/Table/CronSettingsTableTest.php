<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CronSettingsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CronSettingsTable Test Case
 */
class CronSettingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CronSettingsTable
     */
    protected $CronSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.CronSettings',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('CronSettings') ? [] : ['className' => CronSettingsTable::class];
        $this->CronSettings = $this->getTableLocator()->get('CronSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CronSettings);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CronSettingsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
