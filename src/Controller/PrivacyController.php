<?php
// src/Controller/ArticlesController.php

namespace App\Controller;

use App\Controller\AppController;

class PrivacyController extends AppController {
  public function initialize(): void {
    
    parent::initialize();
  }

  public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['index']);
    }

  public function index() {}
  
}