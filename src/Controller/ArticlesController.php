<?php
// src/Controller/ArticlesController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\Table;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\View\CellTrait;
use Cake\I18n\FrozenTime;

class ArticlesController extends AppController {

  public function initialize(): void {
    
    parent::initialize();

    $this->loadComponent('Paginator');
    $this->loadComponent('Flash');
  }

  public function index() {
    // $articles = $this->Paginator->paginate($this->Articles->find('all')->order(['id' => 'DESC']));
    $this->loadModel('Fbpages');
    $this->loadModel('CronSettings');

    $cron_crawl_setting = $this->CronSettings->find()
                                              ->where(['CronSettings.cron_name' => 'cron_auto_post'])
                                              ->first()
                                              ->toArray();
    
    $cron_status = $cron_crawl_setting['enabled'];

    $articles = $this->Paginator
                        ->paginate(
                          $this->Articles
                                  ->find('all')
                                  ->contain('Articleprocessqueues')
                                  ->where(['Articles.article_type' => 'processed'])
                                  ->order(['lastmod' => 'DESC']), ['limit' => '50']
                                );
    $fbpages = $this->Fbpages->find('all');
    $this->set(compact('articles', 'fbpages', 'cron_status'));
  }

  public function raw() {
    $this->loadModel('CronSettings');

    $cron_crawl_setting = $this->CronSettings->find()
                                              ->where(['CronSettings.cron_name' => 'cron_keyword_matcher']);
    
    foreach($cron_crawl_setting as $cron_prop) { 
      $cron_status = $cron_prop->enabled;
    }
    
    $articles = $this->Paginator
                        ->paginate(
                          $this->Articles
                                  ->find('all')
                                  ->where(['Articles.article_type' => 'raw'])
                                  ->order(['lastmod' => 'DESC']), ['limit' => '50']
                                  );
    $this->set(compact('articles','cron_status'));
  }

  public function unused() {

    $articles = $this->Paginator
                        ->paginate(
                          $this->Articles
                                  ->find('all')
                                  ->where(['Articles.article_type' => 'unused'])
                                  ->order(['lastmod' => 'DESC']), ['limit' => '50']
                                  );
    $this->set(compact('articles'));
  }

  public function published() {
    $this->loadModel('SharedArticles');
    
    $published_articles = $this->Paginator
                        ->paginate(
                          $this->SharedArticles
                                  ->find('all')
                                  ->contain(['Articles', 'Fbpages'])
                                  ->order(['SharedArticles.created' => 'DESC']), ['limit' => '200']
                                  );

    $this->set(compact('published_articles'));
  }

  public function view($id = null) {

    $this->loadModel('Fbpages');
    $this->loadModel('Keywords');
    $this->loadModel('Articleprocessqueues');
    $this->loadModel('SharedArticles');

    $article = $this->Articles->findById($id)
                              ->contain(['Fbpageposts', 'Articleprocessqueues'])
                              ->firstOrFail();

    $articles_shared = $this->SharedArticles->find('all')->where(['SharedArticles.article_id' => $id]);

    $articleprocessqueues = $this->Articleprocessqueues->find('all');

    // pr($article->shared_articles);
    // exit;

    $fbpage_info = [];
    $article_match_keywords = [];
    $fbkeywordids = [];
    // $articles_shared = [];

     /**
      * getting the fbpages
      */
    $article_fbpages_obj = $article->articleprocessqueues;
    $article_sharedarticles_obj = $article->shared_articles;
    

    foreach($article_fbpages_obj as $article_fb_page) {
      $article_fb_page = $this->Fbpages->findById($article_fb_page->fbpage_id);
      foreach($article_fb_page as $fbpage) {
        $fbpage_info[] = $fbpage;

         //, 'SharedArticles.fbpage_id' => $fbpage->id
        // $fbpage_info['shared_articles'] = $article_shared;
      }

      
    }

    // echo "duka<br>";
    // pr($article_shared->toArray());
    // exit;

    /**
     * getting the keywords
     */
    $article_keyword_obj = $article->fbpageposts;
      foreach($article_keyword_obj as $article_keyword) {
        if(!empty($article_keyword->keyword_id)) {
          $fbkeywordids[] = $article_keyword->keyword_id;
        }
    }

    foreach($fbkeywordids as $keywordid) {
      $keyword_objects = $this->Keywords->findById($keywordid);
      foreach($keyword_objects as $keyword_object) {
        $article_match_keywords[] = $keyword_object;
      }
    }
    $the_keywords = array_unique($article_match_keywords);

    $og_meta = $this->metaScraper($article->url) ? $this->metaScraper($article->url) : null;

    if(!empty($og_meta)) {
      $og_url = $og_meta['og:url'] ? $og_meta['og:url'] : null;
      $domain_name = parse_url($og_url) != null ? parse_url($og_url) : null;
      $the_site = $domain_name['host'] != null ? $domain_name['host'] : 'Unable to get information from site';
    } else {
      $the_site = null;
    }

    

    $this->set(compact('article', 'og_meta', 'the_site', 'fbpage_info', 'the_keywords', 'articles_shared', 'articleprocessqueues')); 
  }

  public function add() {
    $article = $this->Articles->newEmptyEntity();

    $this->loadModel('Fbpages');
    $fbpages = $this->Fbpages->find('all')->order(['page_title' => 'ASC']);;

    if($this->request->is('post')) {
      $article = $this->Articles->patchEntity($article, $this->request->getData());
      $article->title = $this->articleTitleAdder($article->url);
      $article->fb_caption = $article->fb_caption ? $article->fb_caption : $this->articleCaptionAdder($article->url);
      $article->article_type = isset($article['fbpageposts']) ? "processed" : "raw";

      $time = FrozenTime::now();
      $article->lastmod = $time;

      if($this->Articles->save($article)) {
        if(isset($article['fbpageposts'])) {
          foreach($article['fbpageposts'] as $items) {
            $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
            $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
  
            $articleprocessqueue->article_id = $article->id;
            $articleprocessqueue->fbpage_id = $items['fbpage_id'];            
  
            $articleprocessqueuesTable->save($articleprocessqueue);

            /* Start of Page Group Implementation */
            $page_group_assoc_id = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $items['fbpage_id']],
              'fields' => ['page_group_assoc_id']
            ])->toArray();

            if($page_group_assoc_id[0]['page_group_assoc_id'] != NULL) {
              $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
  
              $articleprocessqueue->article_id = $article->id;
              $articleprocessqueue->fbpage_id = $page_group_assoc_id[0]['page_group_assoc_id'];            
    
              $articleprocessqueuesTable->save($articleprocessqueue);
            }            
            /* End of Page Group Implementation */
          }
        }

        $this->Flash->success(__('New Article is added.'));
        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('Unable to add Article.'));
    }
    $this->set(compact('article', 'fbpages'));
  }

  public function addRaw() {
    $article = $this->Articles->newEmptyEntity();

    $this->loadModel('Fbpages');
    $fbpages = $this->Fbpages->find('all');

    if($this->request->is('post')) {
      $article = $this->Articles->patchEntity($article, $this->request->getData());
      $article->title = $this->articleTitleAdder($article->url);
      $article->article_type = "raw";

      $time = FrozenTime::now();
      $article->lastmod = $time;

      if($this->Articles->save($article)) {
        $this->Flash->success(__('New Raw Article is added.'));
        return $this->redirect(['action' => 'raw']);
      }
      $this->Flash->error(__('Unable to add Article.'));
    }
    $this->set(compact('article', 'fbpages'));
  }

  public function edit($id) {

    $this->loadModel('Fbpages');
    $this->loadModel('Articleprocessqueues');
    $this->loadModel('Fbpageposts');
    
    $fbpages = $this->Fbpages
                      ->find('all')
                      // ->contain('Articleprocessqueues')
                      ->order(['page_title' => 'ASC'])
                      ->toArray();

    $queues_items = $this->Articleprocessqueues->find('all', ['conditions' => ['article_id' => $id]]);
    $fbpagepost_items = $this->Fbpageposts->find('all', ['conditions' => ['article_id' => $id, 'fbpage_id']]);

    // declaring the associated entity
    $associated = ['Articleprocessqueues'];
    // getting the articles table
    $articles = $this->getTableLocator()->get('Articles');
    $article = $articles->find('all', ['contain' => $associated])->where(['id' => $id])->first();
    
    // if update is sent
    if ($this->request->is(['post', 'put'])) {
      // pr($this->request->getData());
      // exit;
      
      foreach($queues_items as $queues_item) {
        $this->Articleprocessqueues->delete($queues_item);
      }

      $this->Articles->patchEntity($article, $this->request->getData(), [
        'associated' => ['Articleprocessqueues']
      ]);

      if(isset($this->request->getData()['articleprocessqueues'])) {
        $article->article_type = 'processed';
      } else {
        $article->article_type = 'raw';
      }

      // if save is successful
      if ($this->Articles->save($article)) {
        if(isset($this->request->getData()['articleprocessqueues'])) {
          $new_fbpages = array();
          foreach($this->request->getData()['articleprocessqueues'] as $data) {
            $new_fbpages[] = $data['fbpage_id'];

            /* Start of Page Group Implementation */
            $page_group_assoc_id = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $data['fbpage_id']],
              'fields' => ['page_group_assoc_id']
            ])->toArray();

            if($page_group_assoc_id[0]['page_group_assoc_id'] != NULL) {
              $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
              $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
  
              $articleprocessqueue->article_id = $id;
              $articleprocessqueue->fbpage_id = $page_group_assoc_id[0]['page_group_assoc_id'];            
    
              $articleprocessqueuesTable->save($articleprocessqueue);
            }            
            /* End of Page Group Implementation */
          }
          
          $fbpagepost_query = $this->Fbpageposts->query();
          $fbpagepost_query->delete()
              ->where(['article_id' => $id, ['fbpage_id NOT IN ' => $new_fbpages]])
              ->execute();
        }

        $this->Flash->success(__('Article has been updated.'));
        return $this->redirect(['action' => 'view', $article->id]);
      }

      $this->Flash->error(__('Unable to update your article.'));
    }
    
    $this->set(compact('article', 'fbpages'));
  }

  public function delete($id) {
    $this->loadModel('Fbpageposts');
    $queues_items = $this->Fbpageposts->find('all');

    $this->request->allowMethod(['post', 'delete']);

    foreach($queues_items as $queues_item) {
      if($queues_item['article_id'] == $id) {
        $this->Fbpageposts->delete($queues_item);
      }
    }

    $article = $this->Articles->findById($id)->firstOrFail();
    if ($this->Articles->delete($article)) {
        $this->Flash->success(__('The {0} article has been deleted.', $article->title));
        return $this->redirect(['action' => 'index']);
    }
  }

  /**
   *  This function will perform Article Soft Delete
   *
   * @param [type] $article_id
   * @return void
   */
  public function deleteArticle($article_id) {
    $articlesTable = $this->getTableLocator()->get('Articles');
    $article = $articlesTable->get($article_id);

    $article->article_type = 'deleted';
    $articlesTable->save($article);

    $this->Flash->success(__('The {0} article has been deleted.', $article->title));
    return $this->redirect($this->referer());
  }

  /**
   *  This function is for manual posting a specific article to Facebook
   *
   * @param [type] $article_id
   * @return void
   */
  public function postToFacebook_old($article_id) {
    $this->loadModel('Fbpages');
    
    $article = $this->Articles->findById($article_id)
                              ->contain(['Articleprocessqueues'])
                              ->firstOrFail();

    $fb_info = [];

    foreach($article->articleprocessqueues as $articleprocessqueue) {
      $fb_objects = $this->Fbpages->findById($articleprocessqueue->fbpage_id);
      foreach($fb_objects as $fb_object) {
        $fb_object->apq_id = $articleprocessqueue->id;
        $fb_info[] = $fb_object;
      }

      $share_caption = $article->fb_caption ? $article->fb_caption : $this->articleCaptionAdder($article->url);
    }

    $fb_unique = array_unique($fb_info);

    foreach($fb_unique as $fb_detail) {
      $this->fbPoster($article->url, $share_caption, $fb_detail['fb_access_token'], $article->id, $fb_detail['page_title'], $fb_detail['id'], $fb_detail['apq_id'], $fb_detail['fb_page_group'], $fb_detail['pageid']);
    }

  }

  /**
   *  This function is for manual posting a specific article to Facebook
   *
   * @param [type] $article_id, $fbpage_id, $apq_id
   * @return void
   */
  public function postToFacebook($article_id, $fbpage_id, $apq_id) {
    $this->loadModel('Fbpages');
    
    $article = $this->Articles->findById($article_id)
                              ->firstOrFail()
                              ->toArray();

    $share_caption = $article['fb_caption'] ? $article['fb_caption'] : $this->articleCaptionAdder($article['url']);

    $fb_detail = $this->Fbpages->find('all')
                               ->where(['Fbpages.id' => $fbpage_id])
                               ->first()->toArray();

    $this->fbPoster($article['url'], $share_caption, $fb_detail['fb_access_token'], $article_id, $fb_detail['page_title'], $fb_detail['id'], $apq_id, $fb_detail['fb_page_group'], $fb_detail['pageid']);
    
    // $this->Flash->success(__('Articles has been published to ' . $fb_detail['page_title'] . ' successfully.'));
    return $this->redirect($this->referer());
  }

  public function postAllToFacebook() {
    $this->loadModel('Fbpages');

    $articles = $this->Articles
                            ->find('all')
                            ->where(['Articles.article_type' => 'processed'])
                            ->order(['lastmod' => 'ASC'])
                            ->contain(['Articleprocessqueues']);
                            // add additional condition making sure article is not shared to fb yet

    $fb_pages = [];

    foreach($articles as $article) {

      foreach($article->articleprocessqueues as $queue) {
        $share_caption = $article->fb_caption ? $article->fb_caption : $this->articleCaptionAdder($article->url);
        $var = $this->is_in_array($fb_pages, 'article_id', $article->id . $queue->fbpage_id);
        $fb_pages[] = array('article_id' => $article->id . $queue->fbpage_id);
        
        if($var == 'no') {
          if($queue->article_id == $article->id) {
          $fb_objects = $this->Fbpages->findById($queue->fbpage_id)->toArray();
            foreach($fb_objects as $fb_object) {
              $this->shareAllToFb($article->url, $share_caption, $fb_object['fb_access_token'], $article->id, $fb_object['page_title'], $fb_object['id'], $queue->id);
            }
          }
        }
      }
    }

    $this->Flash->success(__('All Processed Articles has been published.'));
    return $this->redirect($this->referer());
  }

  /**
   *  This function will be called by Cron to automate Facebook Sharing.
   *
   * @return void
   */
  public function postArticleToFacebook() {
    $this->loadModel('CronSettings');
    $cron_crawl_setting = $this->CronSettings->find()
                                              ->where(['CronSettings.cron_name' => 'cron_auto_post'])
                                              ->first()
                                              ->toArray();
    
    if($cron_crawl_setting['enabled']) {
      $this->loadModel('Fbpages');
      $articles = $this->Articles->find('all')
                              ->where(['Articles.article_type' => 'processed'])
                              ->order(['lastmod' => 'DESC'])
                              ->contain(['Articleprocessqueues']);

      foreach($articles as $article) {
        if(!empty($article)) {
          foreach($article['articleprocessqueues'] as $fb_page) {
            if($fb_page['shared_status'] == 0 && $fb_page['error_message'] == NULL) {
              $fb_objects = $this->Fbpages->find('all')
                                          ->where(['Fbpages.id' => $fb_page['fbpage_id']])
                                          ->first()->toArray();

              $share_caption = $article->fb_caption ? $article->fb_caption : $this->articleCaptionAdder($article['url']);

              if($this->isNowInFbSched($fb_page['fbpage_id'])) {
                $fb_detail = $this->Fbpages->find('all')
                                          ->where(['Fbpages.id' => $fb_page['fbpage_id']])
                                          ->first()->toArray();

                $result = $this->fbCronAutoPoster($article['url'], $share_caption, $fb_detail['fb_access_token'], $article['id'], $fb_detail['page_title'], $fb_detail['id'], $fb_page['id'], $fb_detail['fb_page_group'], $fb_detail['pageid']);
                
                if($result) {
                  break 2;
                }
              }
            }
          }
        }
      }
    }
    $this->autoRender = false;
  }

  public function is_in_array($array, $key, $key_value){
    $within_array = 'no';
    foreach( $array as $k=>$v ){
      if( is_array($v) ){
          $within_array = $this->is_in_array($v, $key, $key_value);
          if( $within_array == 'yes' ){
              break;
          }
      } else {
              if( $v == $key_value && $k == $key ){
                      $within_array = 'yes';
                      break;
              }
      }
    }
      return $within_array;
  }

  public function clearArticles() {
    $this->loadModel('Articles');
    $this->loadModel('Fbpageposts');
    $this->loadModel('Articleprocessqueues');
    $this->loadModel('SharedArticles');
    $articles = $this->Articles->find('all');
    $fbpageposts = $this->Fbpageposts->find('all');
    $articleprocessqueues = $this->Articleprocessqueues->find('all');
    $sharedarticles = $this->SharedArticles->find('all');

    $this->request->allowMethod(['post', 'delete']);

    foreach($articles as $article) {
        // echo "article_id: " . $article->id . "<br>";
        $article = $this->Articles->findById($article->id)->firstOrFail();
        $this->Articles->delete($article);
    }

    foreach($fbpageposts as $fbpagepost) {
      // echo "article_id: " . $article->id . "<br>";
      $article = $this->Fbpageposts->findById($fbpagepost->id)->firstOrFail();
      $this->Fbpageposts->delete($fbpagepost);
    }

    foreach($articleprocessqueues as $articleprocessqueue) {
      // echo "article_id: " . $article->id . "<br>";
      $article = $this->Articleprocessqueues->findById($articleprocessqueue->id)->firstOrFail();
      $this->Articleprocessqueues->delete($articleprocessqueue);
    }

    foreach($sharedarticles as $sharedarticle) {
      // echo "article_id: " . $article->id . "<br>";
      $article = $this->SharedArticles->findById($sharedarticle->id)->firstOrFail();
      $this->SharedArticles->delete($sharedarticle);
    }

    $this->Flash->success(__('All Test Articles cleared.'));
    return $this->redirect($this->referer());
  }

  public function clearAllUnusedArticles() {
    $this->loadModel('Articles');
    
    $success = $this->Articles->updateAll(
            [
              'Articles.article_type' => 'deleted'
            ],
            [
              'Articles.article_type' => 'unused'
            ]
          );

    if($success) {
      $this->Flash->success(__('All Unused Articles are deleted.'));
    } else {
      $this->Flash->success(__('No Unused Articles to be deleted.'));
    }

    return $this->redirect($this->referer());
  }

  public function crawlArticle($article_id) {
    $this->loadModel('Articles');
    $article_result = $this->Articles->findById($article_id)->firstOrFail();

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $article_result->url);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    
    preg_match('/<div class="news_body">(.*?)<\/div>/s', $contents, $match);
    $body = $match[1];

    $keywordsTable = $this->getTableLocator()->get('Keywords');

    $results = $keywordsTable->find('all')->toArray();

    $keyword_found = 0;
    $fb_pages = [];
    
    foreach($results as $result):
      if(stripos($body, $result['keyword']) !== false){
        $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
          'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
          'fields' => ['fbpage_id']
        ])->toArray();

        if(count($fbpagekeywords) > 0) {
          $keyword_found = 1;

          foreach($fbpagekeywords as $keywords):
            $fbpage = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $keywords['fbpage_id']],
              'fields' => ['status']
            ])->first();

            if($fbpage->status == 1) {
              $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
              $fbpagepost = $fbpagepostTable->newEmptyEntity();
    
              $fbpagepost->fbpage_id = $keywords['fbpage_id'];
              $fbpagepost->keyword_id = $result['id'];
              $fbpagepost->article_id = $article_id;
    
              $fbpagepostTable->save($fbpagepost);
    
              $article = $this->Articles->get($article_id);
    
              $article->article_type = 'processed';
              $this->Articles->save($article);
    
              $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_id . $keywords['fbpage_id']);
              $fb_pages[] = array('article_id' => $article_id . $keywords['fbpage_id']);
              
              if($is_in_array == 'no') {
                $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
    
                $articleprocessqueue->article_id = $article_id;
                $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            
    
                $articleprocessqueuesTable->save($articleprocessqueue);
              }
            }  
          endforeach;
        }
      }
    endforeach;

    if($keyword_found == 0) {
      $article = $this->Articles->get($article_id);

      $article->article_type = 'unused';
      $this->Articles->save($article);
    }
    
    // $this->Flash->success(__('The page has been keyword crawled, kindly check <b>Articles.</b>'), ['escape' => false]);
    return $this->redirect($this->referer());
  }

  public function wordpressCrawler($article_id) {
    require_once(ROOT . DS  . 'vendor' . DS  . 'simple_html_dom' . DS . 'simple_html_dom.php');
    
    $this->loadModel('Articles');
    $article_result = $this->Articles->findById($article_id)->firstOrFail();

    $parse = parse_url($article_result->url);
    $html = file_get_html($article_result->url);

    $body = '';
    if($parse['host'] == 'www.vipnieuws.be') {
      foreach($html->find('div.tagdiv-type p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'entertainment-today.be') {
      foreach($html->find('div.entry-content p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showbizznieuws247.be') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'nieuws247.be') {
      foreach($html->find('div.entry-content p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'vipnieuws.nl') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showbizz365.be') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'entertainment-today.nl') {
      foreach($html->find('div.entry-content p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showbizz365.nl') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'mudoo.be') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showbizznieuws247.nl') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showbizz247.nl') {
      foreach($html->find('div.entry-content p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showbiss.nl') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'show24.be') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'show24.nl') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }
    
    if($parse['host'] == 'soapkijker.be') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'royalty247.nl') {
      foreach($html->find('div.entry-content p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'vandaag247.nl') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'vandaag247.be') {
      foreach($html->find('article.single p') as $e)
        $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'happy247.be') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'happy247.nl') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showcafe.be') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showcafe.nl') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'redactie247.be') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'redactie247.nl') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'showboulevard.nl') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'mudoo.nl') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'nl.nieuws-247.be') {
        foreach($html->find('div.entry-content p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'hbvv.be') {
        foreach($html->find('article.single p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'redactie365.be') {
        foreach($html->find('div.entry-content p') as $e)
          $body .= $e->innertext . '<br>';
    }

    if($parse['host'] == 'royalty247.be') {
      foreach($html->find('div.entry-content p') as $e)
        $body .= $e->innertext . '<br>';
  }

    $keywordsTable = $this->getTableLocator()->get('Keywords');

    $results = $keywordsTable->find('all')->toArray();

    $keyword_found = 0;
    $fb_pages = [];
    
    foreach($results as $result):
      if(stripos($body, $result['keyword']) !== false){
        $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
          'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
          'fields' => ['fbpage_id']
        ])->toArray();

        if(count($fbpagekeywords) > 0) {
          $keyword_found = 1;

          foreach($fbpagekeywords as $keywords):
            $fbpage = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $keywords['fbpage_id']],
              'fields' => ['status']
            ])->first();

            if($fbpage->status == 1) {
              $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
              $fbpagepost = $fbpagepostTable->newEmptyEntity();
    
              $fbpagepost->fbpage_id = $keywords['fbpage_id'];
              $fbpagepost->keyword_id = $result['id'];
              $fbpagepost->article_id = $article_id;
    
              $fbpagepostTable->save($fbpagepost);
    
              $article = $this->Articles->get($article_id);
    
              $article->article_type = 'processed';
              $this->Articles->save($article);
    
              $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_id . $keywords['fbpage_id']);
              $fb_pages[] = array('article_id' => $article_id . $keywords['fbpage_id']);
              
              if($is_in_array == 'no') {
                $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
    
                $articleprocessqueue->article_id = $article_id;
                $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            
    
                $articleprocessqueuesTable->save($articleprocessqueue);
              }
            }
          endforeach;
        }
      }
    endforeach;

    if($keyword_found == 0) {
      $article = $this->Articles->get($article_id);

      $article->article_type = 'unused';
      $this->Articles->save($article);
    }
    
    // $this->Flash->success(__('The page has been keyword crawled, kindly check <b>Articles.</b>'), ['escape' => false]);
    return $this->redirect($this->referer());
  }

  public function msnCrawler($article_id) {
    require_once(ROOT . DS  . 'vendor' . DS  . 'simple_html_dom' . DS . 'simple_html_dom.php');
    
    $this->loadModel('Articles');
    $article_result = $this->Articles->findById($article_id)->firstOrFail();

    $html = file_get_html($article_result->url);

    $parse = parse_url($article_result->url);
    $path = explode('/', $parse['path']);

    $body = '';

    if(in_array('fotos', $path)) {
      $dom = new \DOMDocument;
      @ $dom->loadHTML($html);

      foreach ($dom->getElementsByTagName('div') as $tag) {
        if ($tag->getAttribute('class') === 'content') {
          $body .= $tag->nodeValue . '<br />';
        }
      }
    } else {
      foreach($html->find('p') as $e)
        $body .= $e->innertext . '<br>';
    }

    $keywordsTable = $this->getTableLocator()->get('Keywords');

    $results = $keywordsTable->find('all')->toArray();

    $keyword_found = 0;
    $fb_pages = [];
    
    foreach($results as $result):
      if(stripos($body, $result['keyword']) !== false){
        $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
          'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
          'fields' => ['fbpage_id']
        ])->toArray();

        if(count($fbpagekeywords) > 0) {
          $keyword_found = 1;

          foreach($fbpagekeywords as $keywords):
            $fbpage = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $keywords['fbpage_id']],
              'fields' => ['status']
            ])->first();

            if($fbpage->status == 1) {
              $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
              $fbpagepost = $fbpagepostTable->newEmptyEntity();
    
              $fbpagepost->fbpage_id = $keywords['fbpage_id'];
              $fbpagepost->keyword_id = $result['id'];
              $fbpagepost->article_id = $article_id;
    
              $fbpagepostTable->save($fbpagepost);
    
              $article = $this->Articles->get($article_id);
    
              $article->article_type = 'processed';
              $this->Articles->save($article);
    
              $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_id . $keywords['fbpage_id']);
              $fb_pages[] = array('article_id' => $article_id . $keywords['fbpage_id']);
              
              if($is_in_array == 'no') {
                $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
    
                $articleprocessqueue->article_id = $article_id;
                $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            
    
                $articleprocessqueuesTable->save($articleprocessqueue);
              }
            }
          endforeach;
        }
      }
    endforeach;

    if($keyword_found == 0) {
      $article = $this->Articles->get($article_id);

      $article->article_type = 'unused';
      $this->Articles->save($article);
    }
    
    // $this->Flash->success(__('The page has been keyword crawled, kindly check <b>Articles.</b>'), ['escape' => false]);
    return $this->redirect($this->referer());
  }

  public function sport247Crawler($article_id) {
    require_once(ROOT . DS  . 'vendor' . DS  . 'simple_html_dom' . DS . 'simple_html_dom.php');
    
    $this->loadModel('Articles');
    $article_result = $this->Articles->findById($article_id)->firstOrFail();

    $html = file_get_html($article_result->url);

    $body = '';
    foreach($html->find('div.entry-content p') as $e)
      $body .= $e->innertext . '<br>';

    $keywordsTable = $this->getTableLocator()->get('Keywords');

    $results = $keywordsTable->find('all')->toArray();

    $keyword_found = 0;
    $fb_pages = [];
    
    foreach($results as $result):
      if(stripos($body, $result['keyword']) !== false){
        $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
          'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
          'fields' => ['fbpage_id']
        ])->toArray();

        if(count($fbpagekeywords) > 0) {
          $keyword_found = 1;

          foreach($fbpagekeywords as $keywords):
            $fbpage = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $keywords['fbpage_id']],
              'fields' => ['status']
            ])->first();

            if($fbpage->status == 1) {
              $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
              $fbpagepost = $fbpagepostTable->newEmptyEntity();
    
              $fbpagepost->fbpage_id = $keywords['fbpage_id'];
              $fbpagepost->keyword_id = $result['id'];
              $fbpagepost->article_id = $article_id;
    
              $fbpagepostTable->save($fbpagepost);
    
              $article = $this->Articles->get($article_id);
    
              $article->article_type = 'processed';
              $this->Articles->save($article);
    
              $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_id . $keywords['fbpage_id']);
              $fb_pages[] = array('article_id' => $article_id . $keywords['fbpage_id']);
              
              if($is_in_array == 'no') {
                $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
    
                $articleprocessqueue->article_id = $article_id;
                $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            
    
                $articleprocessqueuesTable->save($articleprocessqueue);
              }
            }
          endforeach;
        }
      }
    endforeach;

    if($keyword_found == 0) {
      $article = $this->Articles->get($article_id);

      $article->article_type = 'unused';
      $this->Articles->save($article);
    }
    
    // $this->Flash->success(__('The page has been keyword crawled, kindly check <b>Articles.</b>'), ['escape' => false]);
    return $this->redirect($this->referer());
  }

  public function showbizzsitenlCrawler($article_id) {
    require_once(ROOT . DS  . 'vendor' . DS  . 'simple_html_dom' . DS . 'simple_html_dom.php');
    
    $this->loadModel('Articles');
    $article_result = $this->Articles->findById($article_id)->firstOrFail();

    $html = file_get_html($article_result->url);

    $body = '';
    foreach($html->find('article p') as $e)
      $body .= $e->innertext . '<br>';

    $keywordsTable = $this->getTableLocator()->get('Keywords');

    $results = $keywordsTable->find('all')->toArray();

    $keyword_found = 0;
    $fb_pages = [];
    
    foreach($results as $result):
      if(stripos($body, $result['keyword']) !== false){
        $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
          'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
          'fields' => ['fbpage_id']
        ])->toArray();

        if(count($fbpagekeywords) > 0) {
          $keyword_found = 1;

          foreach($fbpagekeywords as $keywords):
            $fbpage = $this->getTableLocator()->get('Fbpages')->find('all', [
              'conditions' => ['Fbpages.id' => $keywords['fbpage_id']],
              'fields' => ['status']
            ])->first();

            if($fbpage->status == 1) {
              $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
              $fbpagepost = $fbpagepostTable->newEmptyEntity();
    
              $fbpagepost->fbpage_id = $keywords['fbpage_id'];
              $fbpagepost->keyword_id = $result['id'];
              $fbpagepost->article_id = $article_id;
    
              $fbpagepostTable->save($fbpagepost);
    
              $article = $this->Articles->get($article_id);
    
              $article->article_type = 'processed';
              $this->Articles->save($article);
    
              $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_id . $keywords['fbpage_id']);
              $fb_pages[] = array('article_id' => $article_id . $keywords['fbpage_id']);
              
              if($is_in_array == 'no') {
                $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();
    
                $articleprocessqueue->article_id = $article_id;
                $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            
    
                $articleprocessqueuesTable->save($articleprocessqueue);
              }
            }
          endforeach;
        }
      }
    endforeach;

    if($keyword_found == 0) {
      $article = $this->Articles->get($article_id);

      $article->article_type = 'unused';
      $this->Articles->save($article);
    }
    
    // $this->Flash->success(__('The page has been keyword crawled, kindly check <b>Articles.</b>'), ['escape' => false]);
    return $this->redirect($this->referer());
  }

  // public function postNowCheck($fbpage_id) {
  //   return $this->isNowInFbSched($fbpage_id);
  // }

  public function cronCrawl() {
    $this->autoRender = false;
    // before the cron executes the function, it checks its settings first if it is enabled or not
    $this->loadModel('CronSettings');
    $cron_crawl_setting = $this->CronSettings->find()
                                              ->where(['CronSettings.cron_name' => 'cron_keyword_matcher']);

    // getting the correct cron setting row
    foreach($cron_crawl_setting as $cron_prop) {
      if($cron_prop->enabled == 1) {
        $article = $this->Articles->find('all')
                                  ->where(['Articles.article_type' => 'raw'])
                                  ->order(['Articles.lastmod' => 'DESC'])
                                  ->first();
        if(isset($article)){
          $parse = parse_url($article->url);

          if($parse['host'] == 'www.showbizzsite.be') {
            $this->crawlArticle($article->id);
          }

          if($parse['host'] == 'www.vipnieuws.be' || $parse['host'] == 'entertainment-today.be' || $parse['host'] == 'showbizznieuws247.be' || $parse['host'] == 'nieuws247.be' || $parse['host'] == 'vipnieuws.nl' || $parse['host'] == 'showbizz365.be' || $parse['host'] == 'entertainment-today.nl' || $parse['host'] == 'showbizz365.nl' || $parse['host'] == 'mudoo.be' || $parse['host'] == 'showbizznieuws247.nl' || $parse['host'] == 'showbizz247.nl' || $parse['host'] == 'showbiss.nl' || $parse['host'] == 'show24.be' || $parse['host'] == 'show24.nl' || $parse['host'] == 'soapkijker.be' || $parse['host'] == 'royalty247.nl' || $parse['host'] == 'vandaag247.nl' || $parse['host'] == 'vandaag247.be' || $parse['host'] == 'happy247.be' || $parse['host'] == 'happy247.nl' || $parse['host'] == 'showcafe.be' || $parse['host'] == 'showcafe.nl' || $parse['host'] == 'redactie247.be' || $parse['host'] == 'redactie247.nl' || $parse['host'] == 'showboulevard.nl' || $parse['host'] == 'mudoo.nl' || $parse['host'] == 'nl.nieuws-247.be' || $parse['host'] == 'hbvv.be' || $parse['host'] == 'redactie365.be' || $parse['host'] == 'royalty247.be') {
            $this->wordpressCrawler($article->id);
          }

          if($parse['host'] == 'www.msn.com') {
            $this->msnCrawler($article->id);
          }

          if($parse['host'] == 'sport247.be') {
            $this->sport247Crawler($article->id);
          }

          if($parse['host'] == 'www.showbizzsite.nl') {
            $this->showbizzsitenlCrawler($article->id);
          }
        }
        
      }
    }
  }

  public function enableKeywordMatcherCron() {
    $cronSettingsTable = $this->getTableLocator()->get('CronSettings');
    $cronSetting = $cronSettingsTable->get(1);

    $cronSetting->enabled = 1;
    if($cronSettingsTable->save($cronSetting)) {
      $this->cronCrawl();
      $this->Flash->success(__('The CRON for <b>Keyword Matchmaking has been enabled.</b>'), ['escape' => false]);
      return $this->redirect($this->referer());
    } 
    $this->Flash->error(__('Unable to start Keyword Matchmaking CRON.'));
  }

  public function disableKeywordMatcherCron() {
    $cronSettingsTable = $this->getTableLocator()->get('CronSettings');
    $cronSetting = $cronSettingsTable->get(1);

    $cronSetting->enabled = 0;
    if($cronSettingsTable->save($cronSetting)) {
      $this->Flash->success(__('The CRON for <b>Keyword Matchmaking has been paused.</b>'), ['escape' => false]);
      return $this->redirect($this->referer());
    } 
    $this->Flash->error(__('Unable to pause Keyword Matchmaking CRON.'));
  }

  public function toggleAutoPostCron($status) {
    $cronSettingsTable = $this->getTableLocator()->get('CronSettings');
    $cronSetting = $cronSettingsTable->get(3);

    $cronSetting->enabled = $status;
    if($cronSettingsTable->save($cronSetting)) {
      // $this->cronCrawl();
      $this->Flash->success(__('The CRON for <b>Facebook Article Auto Post has been ' . ($status ? 'enabled' : 'paused') . '</b>'), ['escape' => false]);
      return $this->redirect($this->referer());
    } 
    $this->Flash->error(__('Unable to start Facebook Article Auto Post CRON.'));
  }

  public function isArticleOld() {
    $this->loadModel('Articles');

    $old_article = $this->Articles->find('all')
                                  ->where(['Articles.article_type' => 'processed'])
                                  ->order(['lastmod' => 'ASC'])
                                  ->first();

    if(isset($old_article)) {
      $now = FrozenTime::now();
      $article_published = new FrozenTime($old_article->lastmod);
      $expire_datetime = $now->subDays(1);
  
      if($expire_datetime->toUnixString() > $article_published->toUnixString()) {
        $articlesTable = $this->getTableLocator()->get('Articles');
        $article = $articlesTable->get($old_article->id);
  
        $article->article_type = 'unused';
        $articlesTable->save($article);
  
        // $this->Flash->success(__('Article <b>{0}</b> has been moved to <b>Unused Articles</b>', h($old_article->title)), ['escape' => false]);
        
      }
      exit;
    }

    

  }
  
}