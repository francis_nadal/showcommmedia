<?php
// src/Controller/CrawlersController.php

namespace App\Controller;

use App\Controller\AppController;

class CrawlersController extends AppController {

  public function initialize(): void {
    
    parent::initialize();

    $this->loadComponent('Paginator');
    $this->loadComponent('Flash');
  }

  public function index() {
    $this->loadModel('CronSettings');

    $cron_crawl_setting = $this->CronSettings->find()
                                              ->where(['CronSettings.cron_name' => 'cron_xml_feed_crawler']);
    
    foreach($cron_crawl_setting as $cron_prop) { 
      $cron_status = $cron_prop->enabled;
    }

    $crawlers = $this->Paginator->paginate($this->Crawlers->find());
    // var_dump($Crawlers);
    $this->set(compact('crawlers', 'cron_status'));
  }

  public function view($id = null) {
    $crawler = $this->Crawlers->findById($id)->firstOrFail();
    $this->set(compact('crawler'));
  }

  public function add() {
    $crawler = $this->Crawlers->newEmptyEntity();

    if($this->request->is('post')) {
      $crawler = $this->Crawlers->patchEntity($crawler, $this->request->getData());

      if($this->Crawlers->save($crawler)) {
        $this->Flash->success(__('New Feed to crawl was added.'));
        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('Unable to add Feed.'));
    }
    $this->set('crawler', $crawler);
  }

  public function edit($id) {
    $crawler = $this->Crawlers->findById($id)->firstOrFail();

    if ($this->request->is(['post', 'put'])) {
        $this->Crawlers->patchEntity($crawler, $this->request->getData());
        if ($this->Crawlers->save($crawler)) {
            $this->Flash->success(__('Crawler has been updated.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Unable to update your article.'));
    }

    $this->set('crawler', $crawler);
  }

  public function delete($id) {
    $this->request->allowMethod(['post', 'delete']);

    $crawler = $this->Crawlers->findById($id)->firstOrFail();
    if ($this->Crawlers->delete($crawler)) {
        $this->Flash->success(__('The {0} article has been deleted.', $crawler->name));
        return $this->redirect(['action' => 'index']);
    }
  }

  public function crawl($url_feed_id) {
    $this->loadModel('Crawlers');
    // before the cron executes the function, it checks its settings first if it is enabled or not
    $this->loadModel('CronSettings');
    
    $cron_crawl_setting = $this->CronSettings->find()
                                              ->where(['CronSettings.cron_name' => 'cron_xml_feed_crawler']);

    // getting the correct cron setting row
    foreach($cron_crawl_setting as $cron_prop) {
      if($cron_prop->enabled == 1) {
        
        $url_feed = $this->Crawlers->findById($url_feed_id)->firstOrFail();
        $objXmlDocument = simplexml_load_file($url_feed->feed_url);
    
        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
            foreach(libxml_get_errors() as $error) {
                echo $error->message;
            }
            exit;
        }
        
        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
    
        if(isset($arrOutput['url'][0])) {
          for($i = 0; $i < count($arrOutput['url']); $i++):
            $articlesTable = $this->getTableLocator()->get('Articles');
            $article = $articlesTable->newEmptyEntity();
      
            $result = $articlesTable->find('all', [
              'conditions' => ['Articles.url' => $arrOutput['url'][$i]['loc']]
            ]);
      
            if($result->count() == 0) {
              $article->title = $this->articleTitleAdder($arrOutput['url'][$i]['loc']);
              $article->url = $arrOutput['url'][$i]['loc'];
              $article->article_type = 'raw';
              $article->lastmod = $arrOutput['url'][$i]['lastmod'];
              $domain = parse_url($arrOutput['url'][$i]['loc']);
              $article->website_name = $domain['host'];
              
              $articlesTable->save($article);
            }
          endfor;
        } else {
            $articlesTable = $this->getTableLocator()->get('Articles');
            $article = $articlesTable->newEmptyEntity();
      
            $result = $articlesTable->find('all', [
              'conditions' => ['Articles.url' => $arrOutput['url']['loc']]
            ]);
      
            if($result->count() == 0) {
              $article->title = $this->articleTitleAdder($arrOutput['url']['loc']);
              $article->url = $arrOutput['url']['loc'];
              $article->article_type = 'raw';
              $article->lastmod = $arrOutput['url']['lastmod'];
              $domain = parse_url($arrOutput['url']['loc']);
              $article->website_name = $domain['host'];
              
              $articlesTable->save($article);
            }
        }
    
        $this->Flash->success(__('XML Feed Crawl Successfull.'));
        return $this->redirect($this->referer());


      } else {
        $this->Flash->error(__('XML Feed Crawl setting is turned off, please enable it first.'));
        return $this->redirect($this->referer());
      }
    }
  }

  public function crawlpages() {
    $articlesTable = $this->getTableLocator()->get('Articles');
    $article = $articlesTable->newEmptyEntity();

    $article_results = $articlesTable->find('all', [
      'conditions' => ['Articles.article_type' => 'raw']
    ]);

    foreach($article_results as $article_result):
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $article_result['url']);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      
      preg_match('/<div class="col-md-10 col-sm-12 news_body">(.*?)<\/div>/s', $contents, $match);
      $body = $match[1];

      $keywordsTable = $this->getTableLocator()->get('Keywords');

      $results = $keywordsTable->find('all')->toArray();

      $keyword_found = 0;
      $fb_pages = [];

      foreach($results as $result):
        if(strpos($body, $result['keyword']) !== false){
          $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
            'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
            'fields' => ['fbpage_id']
          ])->toArray();
          
          if(count($fbpagekeywords) > 0) {
            $keyword_found = 1;

            foreach($fbpagekeywords as $keywords):
              $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
              $fbpagepost = $fbpagepostTable->newEmptyEntity();

              $fbpagepost->fbpage_id = $keywords['fbpage_id'];
              $fbpagepost->keyword_id = $result['id'];
              $fbpagepost->article_id = $article_result['id'];

              $fbpagepostTable->save($fbpagepost);

              $article = $articlesTable->get($article_result['id']);

              $article->article_type = 'processed';
              $articlesTable->save($article);

              $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_result['id'] . $keywords['fbpage_id']);
              $fb_pages[] = array('article_id' => $article_result['id'] . $keywords['fbpage_id']);
              
              if($is_in_array == 'no') {
                $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();

                $articleprocessqueue->article_id = $article_result['id'];
                $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            

                $articleprocessqueuesTable->save($articleprocessqueue);
              }

            endforeach;
          }
        }
      endforeach;

      if($keyword_found == 0) {
        $article = $articlesTable->get($article_result['id']);

        $article->article_type = 'unused';
        $articlesTable->save($article);
      }

    endforeach;
    
    $this->Flash->success(__('The page has been keyword crawled, kindly check <b>Articles.</b>'), ['escape' => false]);
    return $this->redirect($this->referer());
  }

  public function is_in_array($array, $key, $key_value){
    $within_array = 'no';
    foreach( $array as $k=>$v ){
      if( is_array($v) ){
          $within_array = $this->is_in_array($v, $key, $key_value);
          if( $within_array == 'yes' ){
              break;
          }
      } else {
              if( $v == $key_value && $k == $key ){
                      $within_array = 'yes';
                      break;
              }
      }
    }
      return $within_array;
  }

  public function enableFeedCrawlerCron() {
    $crontSettingsTable = $this->getTableLocator()->get('CronSettings');
    $crontSetting = $crontSettingsTable->get(2);

    $crontSetting->enabled = 1;
    if($crontSettingsTable->save($crontSetting)) {
      // $this->cronCrawl();
      $this->Flash->success(__('The CRON for <b>XML Feed Crawling has been enabled.</b>'), ['escape' => false]);
      return $this->redirect($this->referer());
    } 
    $this->Flash->error(__('Unable to start XML Feed Crawling CRON.'));
  }

  public function disableFeedCrawlerCron() {
    $crontSettingsTable = $this->getTableLocator()->get('CronSettings');
    $crontSetting = $crontSettingsTable->get(2);

    $crontSetting->enabled = 0;
    if($crontSettingsTable->save($crontSetting)) {
      $this->Flash->success(__('The CRON for <b>XML Feed Crawling has been paused.</b>'), ['escape' => false]);
      return $this->redirect($this->referer());
    } 
    $this->Flash->error(__('Unable to pauseXML Feed Crawling CRON.'));
  }

  public function wordpressCrawler() {
    require_once(ROOT . DS  . 'vendor' . DS  . 'simple_html_dom' . DS . 'simple_html_dom.php');

    // $html = file_get_html('https://www.vipnieuws.be/2021/11/29/k3-finaliste-over-winnares-julia-ik-ben-bang-van-jou/');
    // $html = file_get_html('https://www.vipnieuws.be/2021/11/29/nick-kraft-razend-na-k2-zoekt-k3-ik-schaam-mij/');
    $html = file_get_html('https://www.vipnieuws.be/2021/11/29/kathleen-aerts-is-boos-teleurgesteld-moedeloos/');
    

    // find all div tags with class=gbar
    // foreach($html->find('div.tagdiv-type') as $e)
    //     $body = $e->innertext . '<br>';

    $body = '';
    foreach($html->find('div.tagdiv-type p') as $e)
        $body .= $e->innertext . '<br>';

    // $body = $html->find('div.tagdiv-type p', 0)->innertext . '<br>'; // result: "ok"

    // echo 'The Content: ' . $body;

    $keywordsTable = $this->getTableLocator()->get('Keywords');

    $results = $keywordsTable->find('all')->toArray();

    $keyword_found = 0;
    $fb_pages = [];

    foreach($results as $result):
      if(strpos($body, $result['keyword']) !== false){
        echo 'Found: ' . $result['keyword'] . '<br />';
        // $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords')->find('all', [
        //   'conditions' => ['Fbpagekeywords.keyword_id' => $result['id']],
        //   'fields' => ['fbpage_id']
        // ])->toArray();
        
        // if(count($fbpagekeywords) > 0) {
        //   $keyword_found = 1;

        //   foreach($fbpagekeywords as $keywords):
        //     $fbpagepostTable = $this->getTableLocator()->get('Fbpageposts');
        //     $fbpagepost = $fbpagepostTable->newEmptyEntity();

        //     $fbpagepost->fbpage_id = $keywords['fbpage_id'];
        //     $fbpagepost->keyword_id = $result['id'];
        //     $fbpagepost->article_id = $article_result['id'];

        //     $fbpagepostTable->save($fbpagepost);

        //     $article = $articlesTable->get($article_result['id']);

        //     $article->article_type = 'processed';
        //     $articlesTable->save($article);

        //     $is_in_array = $this->is_in_array($fb_pages, 'article_id', $article_result['id'] . $keywords['fbpage_id']);
        //     $fb_pages[] = array('article_id' => $article_result['id'] . $keywords['fbpage_id']);
            
        //     if($is_in_array == 'no') {
        //       $articleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
        //       $articleprocessqueue = $articleprocessqueuesTable->newEmptyEntity();

        //       $articleprocessqueue->article_id = $article_result['id'];
        //       $articleprocessqueue->fbpage_id = $keywords['fbpage_id'];            

        //       $articleprocessqueuesTable->save($articleprocessqueue);
        //     }

        //   endforeach;
        // }
      }
    endforeach;

    exit;
  }
}