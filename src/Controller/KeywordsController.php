<?php
// src/Controller/KeywordsController.php

namespace App\Controller;

use App\Controller\AppController;

class KeywordsController extends AppController {

  public function initialize(): void {
    
    parent::initialize();

    $this->loadComponent('Paginator');
    $this->loadComponent('Flash');
  }

  public function index() {
        $keywords = $this->Paginator->paginate($this->Keywords->find('all')
                                                                ->contain(['Fbpagekeywords'])
                                                                ->order(['keyword' => 'ASC']));
        // var_dump($keywords);
        $this->set(compact('keywords'));
  }

  public function view($id = null) {
    $this->loadModel('Fbpages');

    $keyword = $this->Keywords->findById($id)
                              ->contain(['Fbpagekeywords'])
                              ->firstOrFail();

    $fb_info = [];
    
    foreach($keyword->fbpagekeywords as $fbpagekeyword) {
      $fb_objects = $this->Fbpages->findById($fbpagekeyword->fbpage_id);
      foreach($fb_objects as $fb_object) {
        $fb_info[] = $fb_object;
      }
    }
    $this->set(compact('keyword', 'fb_info'));
  }

  public function add() {
    $keyword = $this->Keywords->newEmptyEntity();

    $this->loadModel('Fbpages');
    $fbpages = $this->Fbpages->find('all')->order(['page_title' => 'ASC']);

    if($this->request->is('post')) {

      // $keywords = $this->Keywords->find('all')->where(['Keywords.keyword' => 'Axel']);
      // foreach($keywords as $keyword_prop) {
      //   // pr($keyword_prop->keyword);
      // }
      // pr($keyword->keyword);
      // exit;
      
      $keyword = $this->Keywords->patchEntity($keyword, $this->request->getData());

      if($this->Keywords->save($keyword)) {
        // echo $keyword->id; exit;
        // echo '<pre>';
        // var_dump($this->request->getData()['fbpagekeywords']); exit;
        // $parent_fbpage_keywords = $this->Fbpagekeywords->find('all', ['conditions' => ['Fbpagekeywords.fbpage_id' => $id]])->toArray();
        
        if(isset($this->request->getData()['fbpagekeywords'])) {

          $fbpage_ids = $this->request->getData()['fbpagekeywords'];
        
          foreach($fbpage_ids as $id) {
            $fbpage = $this->Fbpages->get($id['fbpage_id'], ['fields' => ['Fbpages.page_group_assoc_id']]);
        
            if($fbpage->page_group_assoc_id) {
              $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords');
              $new_data = [
                'fbpage_id' => $fbpage->page_group_assoc_id,
                'keyword_id' => $keyword->id
              ];
              $fbpagekeywords_data = $fbpagekeywords->newEntity($new_data);
              $fbpagekeywords->save($fbpagekeywords_data);
            }
          }
        }

        $this->Flash->success(__('New Keyword was added.'));
        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('Unable to add Keyword.'));
    }
    $this->set(compact('keyword', 'fbpages'));
  }

  public function edit($id) {
    $this->loadModel('Fbpages');
    $this->loadModel('Fbpagekeywords');
    $fbpages = $this->Fbpages->find('all')->order(['page_title' => 'ASC']);;
    $queues_items = $this->Fbpagekeywords->find('all');

    $keyword = $this->Keywords->findById($id)
                              ->contain(['Fbpagekeywords'])
                              ->firstOrFail();

    $current_pages = $this->Fbpagekeywords->find()
      ->select([
        'Fbpagekeywords.fbpage_id',
        'fp.fb_page_group' 
      ])
      ->join([
          'table' => 'fbpages',
          'alias' => 'fp',
          'type' => 'INNER',
          'conditions' => 'Fbpagekeywords.fbpage_id = fp.id',
      ])
      ->where([
          'Fbpagekeywords.keyword_id' => $id,
          // 'fp.fb_page_group' => 'Page'
      ])
      ->toArray();

    // var_dump($current_pages); exit;

    if ($this->request->is(['post', 'put'])) {

      $initial_data = $this->request->getData();

      $new_values = array();
      if(isset($initial_data['fbpagekeywords'])) {
        foreach($initial_data['fbpagekeywords'] as $item) {
          $new_values[] = $item['fbpage_id'];
        }
      }

      $current_values = array();
      foreach($current_pages as $current_page) {
        $current_values[] = $current_page->fbpage_id;
      }

      // var_dump($current_values);
      // echo '<br />';
      // var_dump($new_values);

      $to_be_removed = array_diff($current_values,$new_values);      
      $to_be_added = array_diff($new_values,$current_values);

      // echo '<br />';
      // var_dump($to_be_added);
      // exit;

      $data_fbpagekeywords = isset($initial_data['fbpagekeywords']) ? $initial_data['fbpagekeywords'] : null;

      if($data_fbpagekeywords == null) {
        $the_data_fbpagekeywords = $this->Fbpagekeywords->find('all', [
          'conditions' => ['Fbpagekeywords.keyword_id' => $id]])->toArray();
        $this->Fbpagekeywords->deleteMany($the_data_fbpagekeywords);
      }

      $this->Keywords->patchEntity($keyword, $this->request->getData(), ['associated' => 'Fbpagekeywords']);

      if ($this->Keywords->save($keyword)) {

          foreach($to_be_removed as $item) {
            $fbpage = $this->Fbpages->get($item, ['fields' => ['Fbpages.page_group_assoc_id']]);
    
            if($fbpage->page_group_assoc_id) {
              $to_be_removed_fbpagekeywords = $this->Fbpagekeywords->find('all', [
                'conditions' => ['Fbpagekeywords.fbpage_id' => $fbpage->page_group_assoc_id]])->toArray();
    
              $this->Fbpagekeywords->deleteMany($to_be_removed_fbpagekeywords);
            }
          }

          foreach($to_be_added as $item) {
            $fbpage = $this->Fbpages->get($item, ['fields' => ['Fbpages.page_group_assoc_id']]);
    
            if($fbpage->page_group_assoc_id) {
              $fbpagekeywords = $this->getTableLocator()->get('Fbpagekeywords');
              $new_data = [
                'fbpage_id' => $fbpage->page_group_assoc_id,
                'keyword_id' => $id
              ];
              $fbpagekeywords_data = $fbpagekeywords->newEntity($new_data);
              $fbpagekeywords->save($fbpagekeywords_data);
            }
          }
          
          $this->Flash->success(__('Keyword has been updated.'));
          return $this->redirect($this->referer());
      }
      $this->Flash->error(__('Unable to update your keyword.'));
    }

    $this->set(compact('keyword', 'fbpages'));
  }

  public function delete($id) {
    $this->loadModel('Fbpagekeywords');

    $this->request->allowMethod(['post', 'delete']);

    $keyword = $this->Keywords->findById($id)->firstOrFail();
    if ($this->Keywords->delete($keyword)) {
        $this->Flash->success(__('The {0} keyword has been deleted.', $keyword->keyword));
        return $this->redirect(['action' => 'index']);
    }
  }
}