<?php
// src/Controller/KeywordsController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\FrozenTime;

class FbpagesController extends AppController {

  public function initialize(): void {
    
    parent::initialize();

    $this->loadComponent('Paginator');
    $this->loadComponent('Flash');
  }

  public function index() {
        $fbpages = $this->Paginator->paginate($this->Fbpages->find('all')
                                                              ->where(['status' => 1])
                                                              ->contain(['Fbpagekeywords'])
                                                              ->order(['page_title' => 'ASC']), ['limit' => '200']);

        $fbpage_groups = $this->Fbpages->find('all', ['conditions' => ['status' => 1]]);

        $this->set(compact('fbpages', 'fbpage_groups'));
  }

  public function view($id = null) {
    
    $this->loadModel('Keywords');
    $this->loadModel('Fbpagekeywords');
    $this->loadModel('Fbpagesharetimes');
    $this->loadmodel('Articles');
    $this->loadmodel('SharedArticles');

    $now = FrozenTime::now();
    $date_now = $now->i18nFormat('YYYY-MM-d');

    // pr($date_now);

    $fbpage_match_keywords = [];
    $fbpage_match_sharetimes = [];
    $sorted_keywords = [];

    $fbpage = $this->Fbpages->findById($id)->firstOrFail();

    // $this->isNowInFbSched(5);
    
    $fbpage_keywords = $this->Fbpagekeywords->find('all', [
      'conditions' => ['Fbpagekeywords.fbpage_id' => $id]]);
    $keywords = $this->Keywords->find('all');
    
    $fbpage_sharetimes = $this->Fbpagesharetimes->find('all', [
      'conditions' => ['Fbpagesharetimes.fbpage_id' => $id]]);    
    $fbpagesharetimes = $this->Fbpagesharetimes->find('all');

    $articles_today = $this->SharedArticles->find('all')
                                          ->contain(['Articles'])
                                          ->where([
                                            'SharedArticles.fbpage_id' => $id,
                                            'SharedArticles.created >' => $date_now,
                                          ])
                                          ->order(['SharedArticles.created' => 'DESC']);

    


    // foreach($articles_today as $article_today) {
      
    //   pr($article_today);
    //   // $articles_shared_today[] = $this->Articles->findById($article_today->article_id);
    //   // $articles_shared_today[] = $article;
    // }
    // // pr($articles_shared_today);
    // exit;

    foreach($fbpage_keywords as $fbpage_keyword) {
      
      if(!empty($fbpage_keyword->keyword_id)) {
        foreach($keywords as $keyword) {
          if($keyword->id == $fbpage_keyword->keyword_id) {
            $fbpage_match_keywords[] = $keyword['keyword'];
          }
        }
      }
    }

    sort($fbpage_match_keywords);

    $clength = count($fbpage_match_keywords);
    for($x = 0; $x < $clength; $x++) {
      $sorted_keywords[] = $fbpage_match_keywords[$x];
    }
    
    if($fbpage->page_group_assoc_id) {
      $link_group = $this->Fbpages->findById($fbpage->page_group_assoc_id)->firstOrFail();
      $fbpage->link_group_name = $link_group->page_title;
      $fbpage->link_group_type = $link_group->fb_page_group;
    }

    $this->set(compact('fbpage', 'sorted_keywords', 'fbpage_sharetimes', 'articles_today'));
  }

  public function add() {
    $this->loadModel('Keywords');
    $keywords = $this->Keywords->find('all')->order(['keyword' => 'ASC']);

    $fbpage = $this->Fbpages->newEmptyEntity();

    if($this->request->is('post')) {
      $fbpage = $this->Fbpages->patchEntity($fbpage, $this->request->getData());

      // $fbpage_data = $this->request->getData();
      // pr($fbpage_data['fbpagesharetimes']);
      // $fbpagesharetimes_data = $fbpage_data['fbsharetimes'];
      // // pr($fbsharetime_data);
      // // debug($fbpage);
      // exit;

      if($this->Fbpages->save($fbpage)) {
        $this->Flash->success(__('New FB Page was added.'));
        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error(__('Unable to add FB Page.'));
    }
    $this->set(compact('fbpage', 'keywords'));
  }

  public function edit($id) {
    $this->loadModel('Fbpagekeywords');
    $this->loadModel('Keywords');
    $this->loadModel('Fbpagesharetimes');
    // $this->loadModel('Fbpagegroups');
    $keywords = $this->Keywords->find('all')->order(['keyword' => 'ASC']);
    $fbpage_keywords = $this->Fbpagekeywords->find('all');
    $fbpage_sharetimes = $this->Fbpagesharetimes->find('all');

    // $fbpage = $this->Fbpages->findById($id)->firstOrFail();

    $associated = ['Fbpagekeywords','Fbpagesharetimes'];
    $Fbpages = $this->getTableLocator()->get('Fbpages');
    $fbpage = $Fbpages->get($id, ['contain' => $associated]);
    $fbpage_group = $Fbpages->find('all', ['fields' => ['Fbpages.id', 'Fbpages.page_title'], 'conditions' => ['Fbpages.fb_page_group' => 'Group', 'Fbpages.page_group_assoc_id IS NULL', 'Fbpages.status' => 1], 'order' => ['Fbpages.page_title' => 'ASC']])->toArray();
    
    $fbpage_group_options = [];
    $current_page_group_assoc_id = $fbpage->page_group_assoc_id;

    if($fbpage->page_group_assoc_id) {
      $fbpage_get_group = $Fbpages->get($fbpage->page_group_assoc_id);
      $fbpage_group_options[$fbpage_get_group->id] = $fbpage_get_group->page_title;
    }    

    foreach($fbpage_group as $page_group):
      $fbpage_group_options[$page_group->id] = $page_group->page_title;
    endforeach;
    
    // $fbpage_group = $this->Fbpagegroups->find('all', ['contain' => ['Fbpages'], 'fields' => ['Fbpages.id', 'Fbpages.page_title'], 'conditions' => ['Fbpagegroups.status' => 0]])->toList();

    // $fbpage_group_options = [];
    // foreach($fbpage_group as $page_group):
    //   $fbpage_group_options[$page_group->fbpage->id] = $page_group->fbpage->page_title;
    // endforeach;

    // var_dump($fbpage_group_options);
    // exit;
    // var_dump($fbpage->page_group_assoc_id); exit;

    if ($this->request->is(['post', 'put'])) {

      $initial_data = $this->request->getData();

      // var_dump($initial_data['page_group_assoc_id']); exit;
      // echo $id;
      // var_dump($fbpage); exit;

      $data_fbpagesharetimes = isset($initial_data['fbpagesharetimes']) ? $initial_data['fbpagesharetimes'] : null;
      $data_fbpagekeywords = isset($initial_data['fbpagekeywords']) ? $initial_data['fbpagekeywords'] : null;

      if($data_fbpagesharetimes == null) {
        $the_fbpage_sharetimes = $this->Fbpagesharetimes->find('all', [
          'conditions' => ['Fbpagesharetimes.fbpage_id' => $id]])->toArray();
        $this->Fbpagesharetimes->deleteMany($the_fbpage_sharetimes);
      }

      if($data_fbpagekeywords == null) {
        $the_fbpagekeywords = $this->Fbpagekeywords->find('all', [
          'conditions' => ['Fbpagekeywords.fbpage_id' => $id]])->toArray();
        $this->Fbpagekeywords->deleteMany($the_fbpagekeywords);
      }

      $data = $this->Fbpages->patchEntity($fbpage, $this->request->getData());

      if ($this->Fbpages->save($data)) {
          $this->Flash->success(__('Facebook Page has been updated.'));

          /* Start of Delete Group Association */
          // $fbpages_update_assoc_id = $this->Fbpages->find()->where(['page_group_assoc_id' => $id]);
  
          // $fbpages_update_assoc_id->page_group_assoc_id = NULL;
          // $this->Fbpages->save($fbpages_update_assoc_id);

          // $this->Fbpages->id = $this->Fbpages->field('id', array('page_group_assoc_id' => $id));
          // if ($this->Fbpages->id) {
          //     $this->Fbpages->saveField('page_group_assoc_id', NULL);
          // }

          $this->Fbpages->updateAll(
            array('page_group_assoc_id' => NULL), // set to NULL
            array('page_group_assoc_id' => $id) // where 'page_group_assoc_id' = $id
          );
          /* End of Delete Group Association */

          /* Start of Save New Group Association */
          if($initial_data['page_group_assoc_id']) {
            $fbpages_update_assoc_id = $this->Fbpages->get($initial_data['page_group_assoc_id']);
  
            $fbpages_update_assoc_id->page_group_assoc_id = $id;
            $this->Fbpages->save($fbpages_update_assoc_id);

            /* Start of Duplicate Page Share Times and Page Keywords to group associated */
            if($current_page_group_assoc_id) {
              $the_fbgroup_sharetimes = $this->Fbpagesharetimes->find('all', [
                'conditions' => ['Fbpagesharetimes.fbpage_id' => $current_page_group_assoc_id]])->toArray();
              $this->Fbpagesharetimes->deleteMany($the_fbgroup_sharetimes);
            }            

            $parent_fbpage_sharetimes = $this->Fbpagesharetimes->find('all', ['conditions' => ['Fbpagesharetimes.fbpage_id' => $id]])->toArray();
            foreach($parent_fbpage_sharetimes as $item) {
              $clone_parent_fbpage_sharetimes_table = $this->getTableLocator()->get('Fbpagesharetimes');
              $clone_data = [
                  'fbpage_id' => $initial_data['page_group_assoc_id'],
                  'from_time' => $item->from_time,
                  'to_time' => $item->to_time,
              ];
              $clone = $clone_parent_fbpage_sharetimes_table->newEntity($clone_data);
              $clone_parent_fbpage_sharetimes_table->save($clone);
            }
            /* ------------------------------------------------------------------------------------------------ */
            if($current_page_group_assoc_id) {
              $the_fbgroupkeywords = $this->Fbpagekeywords->find('all', [
                'conditions' => ['Fbpagekeywords.fbpage_id' => $current_page_group_assoc_id]])->toArray();
              $this->Fbpagekeywords->deleteMany($the_fbgroupkeywords);
            }            

            $parent_fbpage_keywords = $this->Fbpagekeywords->find('all', ['conditions' => ['Fbpagekeywords.fbpage_id' => $id]])->toArray();
            foreach($parent_fbpage_keywords as $item) {
              $clone_parent_fbpage_keywords_table = $this->getTableLocator()->get('Fbpagekeywords');
              $clone_data = [
                  'fbpage_id' => $initial_data['page_group_assoc_id'],
                  'keyword_id' => $item->keyword_id
              ];
              $clone = $clone_parent_fbpage_keywords_table->newEntity($clone_data);
              $clone_parent_fbpage_keywords_table->save($clone);
            }
            /* End of Duplicate Page Share Times and Page Keywords to group associated */
          } else {
            if($current_page_group_assoc_id != NULL) {
              // remove Page Share Times records
              $the_fbgroup_sharetimes = $this->Fbpagesharetimes->find('all', [
                'conditions' => ['Fbpagesharetimes.fbpage_id' => $current_page_group_assoc_id]])->toArray();
              $this->Fbpagesharetimes->deleteMany($the_fbgroup_sharetimes);

              // remove Page Keywords records
              $the_fbgroupkeywords = $this->Fbpagekeywords->find('all', [
                'conditions' => ['Fbpagekeywords.fbpage_id' => $current_page_group_assoc_id]])->toArray();
              $this->Fbpagekeywords->deleteMany($the_fbgroupkeywords);
            }
          }
          /* End of Save New Group Association */

          return $this->redirect($this->referer());
      }
      $this->Flash->error(__('Unable to update the Facebook Page.'));
    }

    $this->set(compact('fbpage', 'keywords','fbpage_sharetimes', 'fbpage_group_options'));
  }

  public function generateToken($page_id) {
    $ch = curl_init();

    // $page_id = '345845225486365';
    curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/'.$page_id.'?fields=access_token&access_token='.$this->loggedInUserAccessToken);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $this->autoRender = false;
    echo $result;
  }

  public function delete($id) {
    // $this->request->allowMethod(['post', 'delete']);

    $this->loadModel('Fbpagekeywords');
    $this->loadModel('Fbpagesharetimes');
    $this->loadModel('Articleprocessqueues');

    $fbpagesTable = $this->getTableLocator()->get('Fbpages');
    $fbpage = $fbpagesTable->get($id);

    $link_group_id = $fbpage->page_group_assoc_id;
    $fb_group_type = $fbpage->fb_page_group;

    $fbpage->status = 0;
    $fbpage->page_group_assoc_id = NULL;
    if ($fbpagesTable->save($fbpage)) {
      $this->Fbpages->updateAll(
        array('page_group_assoc_id' => NULL), // set to NULL
        array('page_group_assoc_id' => $id) // where 'page_group_assoc_id' = $id
      );
      
      // delete Fbpage share times and keywords
      if($link_group_id != NULL) {
        $link_fbpagesTable = $this->getTableLocator()->get('Fbpages');
        $link_fbpage = $link_fbpagesTable->get($link_group_id);

        if($link_fbpage->fb_page_group == 'Group') {
          $the_fbgroup_sharetimes = $this->Fbpagesharetimes->find('all', [
            'conditions' => ['Fbpagesharetimes.fbpage_id' => $link_group_id]])->toArray();
          $this->Fbpagesharetimes->deleteMany($the_fbgroup_sharetimes);
  
          $the_fbgroupkeywords = $this->Fbpagekeywords->find('all', [
            'conditions' => ['Fbpagekeywords.fbpage_id' => $link_group_id]])->toArray();
          $this->Fbpagekeywords->deleteMany($the_fbgroupkeywords);

          // delete articleproccessqueue records
          $the_fbgroup_articleprocessqueues = $this->Articleprocessqueues->find('all', [
            'conditions' => ['Articleprocessqueues.fbpage_id' => $link_group_id]])->toArray();
          $this->Articleprocessqueues->deleteMany($the_fbgroup_articleprocessqueues);
        }
      }

      if($fb_group_type == 'Group') {
        $the_fbgroup_sharetimes = $this->Fbpagesharetimes->find('all', [
          'conditions' => ['Fbpagesharetimes.fbpage_id' => $id]])->toArray();
        $this->Fbpagesharetimes->deleteMany($the_fbgroup_sharetimes);

        $the_fbgroupkeywords = $this->Fbpagekeywords->find('all', [
          'conditions' => ['Fbpagekeywords.fbpage_id' => $id]])->toArray();
        $this->Fbpagekeywords->deleteMany($the_fbgroupkeywords);
      }

      // delete articleproccessqueue records
      $the_fbgroup_articleprocessqueues = $this->Articleprocessqueues->find('all', [
        'conditions' => ['Articleprocessqueues.fbpage_id' => $id]])->toArray();
      $this->Articleprocessqueues->deleteMany($the_fbgroup_articleprocessqueues);

      $this->Flash->success(__('The {0} FB Page has been inactivated.', $fbpage->page_title));
      return $this->redirect(['action' => 'index']);
    }

    // $fbpage = $this->Fbpages->findById($id)->firstOrFail();
    // if ($this->Fbpages->delete($fbpage)) {
    //     $this->Flash->success(__('The {0} FB Page has been deleted.', $fbpage->page_title));
    //     return $this->redirect(['action' => 'index']);
    // }
  }
}