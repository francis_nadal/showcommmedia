<?php
// src/Controller/KeywordsController.php

namespace App\Controller;

class StatpostingsController extends AppController {

  public function index() {
        $this->loadComponent('Paginator');
        $statpostings = $this->Paginator->paginate($this->Statpostings->find());
        // var_dump($keywords);
        $this->set(compact('statpostings'));
  }

  public function view($id = null) {
    $statposting = $this->Statpostings->findById($id)->firstOrFail();
    $this->set(compact('statposting'));
  }
}