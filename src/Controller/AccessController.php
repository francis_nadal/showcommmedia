<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Access Controller
 *
 * @method \App\Model\Entity\Access[]]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccessController extends AppController
{
    public function login()
    {
        $result = $this->Authentication->getResult();
        // pr($result);
        // If the user is logged in send them away.
        if ($result->isValid()) {
            
            return $this->redirect(['controller' => 'Articles', 'action' => 'index']);
        }
        if ($this->request->is('post') && !$result->isValid()) {
            // pr($this->request);
            $this->Flash->error('Invalid username or password');
        }
    }

    public function logout()
    {
        $this->Authentication->logout();
        return $this->redirect(['controller' => 'access', 'action' => 'login']);
    }
}
