<?php
// src/Controller/SchedulersController.php

namespace App\Controller;

class SchedulersController extends AppController {

  public function index() {
        $this->loadComponent('Paginator');
        $schedulers = $this->Paginator->paginate($this->Schedulers->find());
        // var_dump($Schedulers);
        $this->set(compact('schedulers'));
  }

  public function view($id = null) {
    $scheduler = $this->Schedulers->findById($id)->firstOrFail();
    $this->set(compact('scheduler'));
  }
}