<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Authentication.Authentication');

        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');
    }


    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated([
            'login',
            'postAllToFacebook',
            'postArticleToFacebook',
            'crawl', 
            'postNowCheck',
            'cronCrawl',
            'isArticleOld'
        ]);

        $data = $this->Authentication->getResult()->getData(); //actual user entity, null when not logged in
        $this->isLoggedIn = $this->Authentication->getResult()->isValid();
        $this->loggedInID = $this->isLoggedIn ? $data->id : -1;
        $this->loggedInEmail = $this->isLoggedIn ? h($data->email) : "";
        $this->loggedInUserAccessToken = $this->isLoggedIn ? h($data->user_access_token) : "";
        $this->loggedInUsername = $this->isLoggedIn ? h($data->username) : ""; // h() to sanitise
        
        $this->set(['isLoggedIn' => $this->isLoggedIn,
                    'loggedInID' => $this->loggedInID,
                    'loggedInEmail' => $this->loggedInEmail,
                    'loggedInUserAccessToken' => $this->loggedInUserAccessToken,
                    'loggedInUsername' => $this->loggedInUsername
                  ]);
    }

    /**
     * This function is for posting Articles to multiple Facebook Pages
     */

    public function fbPoster($url = null, $caption = null, $fbpage_access_token = null, $article_id, $fb_page_title, $fb_page_id, $apq_id, $fb_page_group, $fb_pageid) {

        // echo "url: " .$url."<br>";
        // echo "caption: " .$caption."<br>";
        // echo "access_token: " .$fbpage_access_token."<br>";
        // echo "article_id: " .$article_id."<br>";
        // echo "fb_page_title: " .$fb_page_title."<br>";
        // echo "fb_page_id: " .$fb_page_id."<br>";
        // echo "apq_id: " .$apq_id."<br>";
        // echo "fb_page_group: " .$fb_page_group."<br>";
        // echo "fb_pageid: " .$fb_pageid."<br>";
        // exit;
        
        // if($fbpage_access_token != null) {
            $fb = new \Facebook\Facebook([
            'app_id' => Configure::read('Fbapp.app_id'),
            'app_secret' => Configure::read('Fbapp.app_secret'),
            'default_graph_version' => 'v2.10',
            //'default_access_token' => '{access-token}', // optional
            ]);
        
            $helper = $fb->getRedirectLoginHelper();  
        
            $linkData = [
            'link' => $url,
            'message' => $caption,
            // 'message' => 'test local error',
            ];
        
            try {
            // Returns a `Facebook\FacebookResponse` object
            // $response = $fb->post('/me/feed', $linkData, $fbpage_access_token);
                if($fb_page_group == 'Page') {
                    $response = $fb->post('/me/feed', $linkData, $fbpage_access_token);
                } else {

                    if($this->isLoggedIn != 1) {
                        $this->loadModel('Users');
                        $user_token = $this->Users->get(3)->user_access_token;
                    } else {
                        $user_token = $this->loggedInUserAccessToken;
                    }

                    // $response = $fb->post('/'.$fb_pageid.'/feed', $linkData, $user_token); //original

                    $page_group_assoc_id = $this->getTableLocator()->get('Fbpages')->find('all', [
                        'conditions' => ['Fbpages.id' => $fb_page_id],
                        'fields' => ['page_group_assoc_id']
                    ])->toArray();
                    
                    if($page_group_assoc_id[0]['page_group_assoc_id'] != NULL) {
                        $fb_access_token = $this->getTableLocator()->get('Fbpages')->find('all', [
                            'conditions' => ['Fbpages.id' => $page_group_assoc_id[0]['page_group_assoc_id']],
                            'fields' => ['fb_access_token']
                        ])->toArray();

                        if($fb_access_token[0]['fb_access_token'] != NULL) {
                            $response = $fb->post('/'.$fb_pageid.'/feed', $linkData, $fb_access_token[0]['fb_access_token']);
                        }
                    }

                    // $ars = 'EAAMRqLboVg0BANRPjOGTVJP3PZAnNoMcHOIDvnxecPO5Opd17Is1VgwcaKcZCzSMBwCvdhuWr1JBlgNdQAYWMU9arqXbH7wViWpSFKDwWF7pZCktZBxZCMF0SgvZBbDD3emZBXjqP8bReKaH1I4SZALAITiWY4tPr9wZCVotS4eStGom8PCZCA2hJFbLCBohTKluY8DXZCieO4x8AZDZD';
                    // $response = $fb->post('/'.$fb_pageid.'/feed', $linkData, $ars);
                    // $response = $fb->post('/518864522583053/feed', $linkData, $accessToken)
                }

                $this->Flash->success(__('Articles has been published to ' . $fb_page_title . ' successfully.'));
                
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // echo 'Graph returned an error: ' . $e->getMessage();
            $this->Flash->error(__('The article was not shared to "{0}". Error message: {1}', $fb_page_title, $e->getMessage()));

            $this->updateArticleShareStatus($apq_id, $e->getMessage());

            return $this->redirect(['action' => 'view', $article_id]);
            // exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
            }
        
            $graphNode = $response->getGraphNode();
        
            // echo 'Posted with id: ' . $graphNode['id'];

            $this->loadModel('Articles');

            $SharedArticlesTable = $this->getTableLocator()->get('SharedArticles');
            $SharedArticles = $SharedArticlesTable->newEmptyEntity();

            $SharedArticles->fbpage_id = $fb_page_id;
            $SharedArticles->graph_node_id = $graphNode['id'];
            $SharedArticles->article_id = $article_id;

            $SharedArticlesTable->save($SharedArticles);

            $ArticleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
            $articleprocessqueue = $ArticleprocessqueuesTable->get($apq_id);

            $articleprocessqueue->shared_status = 1;
            $articleprocessqueue->error_message = NULL;
            if($ArticleprocessqueuesTable->save($articleprocessqueue)) {
                $unshared_article_count = $ArticleprocessqueuesTable->find('all')
                    ->where(['Articleprocessqueues.article_id' => $article_id, 'Articleprocessqueues.shared_status' => 0]);

                if($unshared_article_count->count() == 0) {
                    $article = $this->Articles->findById($article_id)->firstOrFail();
                    $this->Articles->patchEntity($article, $this->request->getData());
                    $article->article_type = "published";                    
                    $this->Articles->save($article);
                }
            }

            $this->Flash->success(__('This article has been shared to <b>{0}</b>. Graph Node ID: <b>{1}</b>', h($fb_page_title), $graphNode['id']), ['escape' => false]);
            return $this->redirect($this->referer());

        // } else {
        //     $this->Flash->error(__('Facebook Page <b>{0}</b> possibly do not have/outdated Access Token, please update the Page.', h($fb_page_title)), ['escape' => false]);
        //     return $this->redirect($this->referer());
        // }
    
    }

    /**
     * This function is for posting Articles using Cron Auto Posting
    */

    public function fbCronAutoPoster($url = null, $caption = null, $fbpage_access_token = null, $article_id, $fb_page_title, $fb_page_id, $apq_id, $fb_page_group, $fb_pageid) {
        $fb = new \Facebook\Facebook([
        'app_id' => Configure::read('Fbapp.app_id'),
        'app_secret' => Configure::read('Fbapp.app_secret'),
        'default_graph_version' => 'v2.10'
        ]);
    
        $helper = $fb->getRedirectLoginHelper();  
    
        $linkData = [
        'link' => $url,
        'message' => $caption,
        ];
    
        try {
            if($fb_page_group == 'Page') {
                $response = $fb->post('/me/feed', $linkData, $fbpage_access_token);
            } else {

                if($this->isLoggedIn != 1) {
                    $this->loadModel('Users');
                    $user_token = $this->Users->get(3)->user_access_token;
                } else {
                    $user_token = $this->loggedInUserAccessToken;
                }

                // $response = $fb->post('/'.$fb_pageid.'/feed', $linkData, $user_token);

                $page_group_assoc_id = $this->getTableLocator()->get('Fbpages')->find('all', [
                    'conditions' => ['Fbpages.id' => $fb_page_id],
                    'fields' => ['page_group_assoc_id']
                ])->toArray();
                
                if($page_group_assoc_id[0]['page_group_assoc_id'] != NULL) {
                    $fb_access_token = $this->getTableLocator()->get('Fbpages')->find('all', [
                        'conditions' => ['Fbpages.id' => $page_group_assoc_id[0]['page_group_assoc_id']],
                        'fields' => ['fb_access_token']
                    ])->toArray();

                    if($fb_access_token[0]['fb_access_token'] != NULL) {
                        $response = $fb->post('/'.$fb_pageid.'/feed', $linkData, $fb_access_token[0]['fb_access_token']);
                    }
                }
            }            
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            $this->updateArticleShareStatus($apq_id, $e->getMessage());

            return 0;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return 0;
        }
    
        $graphNode = $response->getGraphNode();

        $this->loadModel('Articles');

        $SharedArticlesTable = $this->getTableLocator()->get('SharedArticles');
        $SharedArticles = $SharedArticlesTable->newEmptyEntity();

        $SharedArticles->fbpage_id = $fb_page_id;
        $SharedArticles->graph_node_id = $graphNode['id'];
        $SharedArticles->article_id = $article_id;

        $SharedArticlesTable->save($SharedArticles);

        $ArticleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
        $articleprocessqueue = $ArticleprocessqueuesTable->get($apq_id);

        $articleprocessqueue->shared_status = 1;
        $articleprocessqueue->error_message = NULL;
        if($ArticleprocessqueuesTable->save($articleprocessqueue)) {
            $unshared_article_count = $ArticleprocessqueuesTable->find('all')
                ->where(['Articleprocessqueues.article_id' => $article_id, 'Articleprocessqueues.shared_status' => 0]);

            if($unshared_article_count->count() == 0) {
                $article = $this->Articles->findById($article_id)->firstOrFail();
                $this->Articles->patchEntity($article, $this->request->getData());
                $article->article_type = "published";                    
                $this->Articles->save($article);
            }
        }

        return 1; 
    }

    public function updateArticleShareStatus($apq_id, $message) {
        $ArticleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
        $articleprocessqueue = $ArticleprocessqueuesTable->get($apq_id);

        $articleprocessqueue->error_message = $message;
        $ArticleprocessqueuesTable->save($articleprocessqueue);
    }

    /**
     * This function is for posting Articles to multiple Facebook Pages
     */

    public function shareAllToFb($url = null, $caption = null, $fbpage_access_token = null, $article_id, $fb_page_title, $fb_page_id, $apq_id) {

        if($fbpage_access_token != null) {
            $fb = new \Facebook\Facebook([
            'app_id' => Configure::read('Fbapp.app_id'),
            'app_secret' => Configure::read('Fbapp.app_secret'),
            'default_graph_version' => 'v2.10',
            //'default_access_token' => '{access-token}', // optional
            ]);
        
            $helper = $fb->getRedirectLoginHelper();  
        
            $linkData = [
            'link' => $url,
            'message' => $caption,
            ];
        
            try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->post('/me/feed', $linkData, $fbpage_access_token);
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // echo 'Graph returned an error: ' . $e->getMessage();
            $this->Flash->error(__('The article was not shared to {0}". Error message: {1}', $fb_page_title, $e->getMessage()));
            return $this->redirect(['action' => 'view', $article_id]);
            // exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
            }
        
            $graphNode = $response->getGraphNode();
        
            // echo 'Posted with id: ' . $graphNode['id'];

            $this->loadModel('Articles');

            $article = $this->Articles->findById($article_id)->firstOrFail();

            $this->Articles->patchEntity($article, $this->request->getData());
            $article->article_type = "published";
            // $article->graphnode_id = $graphNode['id'] ? $graphNode['id'] : "";
            
            if ($this->Articles->save($article)) {
                $SharedArticlesTable = $this->getTableLocator()->get('SharedArticles');
                $SharedArticles = $SharedArticlesTable->newEmptyEntity();
    
                $SharedArticles->fbpage_id = $fb_page_id;
                $SharedArticles->graph_node_id = $graphNode['id'];
                $SharedArticles->article_id = $article_id;
    
                $SharedArticlesTable->save($SharedArticles);

                $ArticleprocessqueuesTable = $this->getTableLocator()->get('Articleprocessqueues');
                $articleprocessqueue = $ArticleprocessqueuesTable->get($apq_id);

                $articleprocessqueue->shared_status = 1;
                $ArticleprocessqueuesTable->save($articleprocessqueue);

                // $this->Flash->success(__('This article has been shared to <b>{0}</b>. Graph Node ID: <b>{1}</b>', h($fb_page_title), $graphNode['id']), ['escape' => false]);
                // $this->Flash->success(__('All Processed Articles has been published.'));
                // return $this->redirect($this->referer());
            }

            // $this->Flash->success(__('All Processed Articles has been published.'));
            // return $this->redirect($this->referer());
        
            // $this->Flash->success(__('This article has been shared to <b>{0}</b>. Graph Node ID: <b>{1}</b>', h($fb_page_title), $graphNode['id']), ['escape' => false]);
            // return $this->redirect(['action' => 'view', $article_id]);

        } else {
            // $this->Flash->error(__('Facebook Page <b>{0}</b> possibly do not have/outdated Access Token, please update the Page.', h($fb_page_title)), ['escape' => false]);
            // return $this->redirect(['action' => 'view', $article_id]);
        }
    
    }

    public function metaScraper($url) {
        $web = new \spekulatius\phpscraper();

        $web->go($url);

        // if(!isset($web->openGraph)) {
        //     return null;
        // } 
        
        return $web->openGraph;
        

        
    }

    public function articleTitleAdder($url) {
        
        $web = new \spekulatius\phpscraper();

        $web->go($url);

        if(isset($web->openGraph['og:title'])) {
            return $web->openGraph['og:title'];    
        }

        return $web->title;
    }

    public function articleCaptionAdder($url) {
        
        $web = new \spekulatius\phpscraper();

        $web->go($url);

        return $web->openGraph['og:description'] ? $web->openGraph['og:description'] : '';
    }

    public function isNowInFbSched($fbpage_id) {

        $this->loadModel('Fbpages');

        $fbpage = $this->Fbpages->findById($fbpage_id)
                                ->contain('Fbpagesharetimes');

        $now = FrozenTime::now();
        $time_now = $now->i18nFormat('HH:mm:ss');
        // echo "time now: " . $time_now . "<br>";

        foreach($fbpage as $fbpage_prop) {
            if(!empty($fbpage_prop->fbpagesharetimes)) {
                foreach($fbpage_prop->fbpagesharetimes as $fbpage_post_time) {
                  $from_time = $fbpage_post_time->from_time;
                  $to_time = $fbpage_post_time->to_time;
                  
                  if($time_now >= $from_time && $time_now <= $to_time) {
                      if($this->intervalAllowed($fbpage_id)) {
                        return true;
                        break;
                      }
                  }
                }
                return false;
            } else {
                return false;
            }
        }
    }

    public function intervalAllowed($fbpage_id) {
        $last_post = $this->getFbPageLastPost($fbpage_id);
        $interval_time = $this->getFbPagePostInterval($fbpage_id);

        if($last_post != NULL) {
            $time_now = FrozenTime::now();

            // echo $time_now . '<br />';

            $last_post_time = FrozenTime::parse($last_post);

            // echo $last_post_time . '<br />';
            if($interval_time != NULL) {
                $interval_time = $last_post_time->addMinutes($interval_time);

                // echo $interval_time . '<br />';

                if($interval_time < $time_now) {
                    // echo 'Its time!!';
                    return true;
                } else {
                    // echo 'Its not time yet!!';
                    return false;
                }
            }
        }
        
        return true;
    }

    public function getFbPagePostInterval($fbpage_id) {
        $this->loadModel('Fbpages');
        $result = $this->Fbpages->find('all')
                                    ->where(['Fbpages.id' => $fbpage_id])
                                    ->first()->toArray();

        return $result['post_interval'];
    }

    public function getFbPageLastPost($fbpage_id) {
        $this->loadModel('SharedArticles');
        $result = $this->SharedArticles->find('all')
                                        ->where(['SharedArticles.fbpage_id' => $fbpage_id])
                                        ->order(['SharedArticles.created' => 'DESC'])
                                        ->first();

        if(!empty($result)) {
            $result->toArray();
            return $result['created'];
        }

        return NULL;
    }
}
