<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CronSettings Model
 *
 * @method \App\Model\Entity\CronSetting newEmptyEntity()
 * @method \App\Model\Entity\CronSetting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\CronSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CronSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\CronSetting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\CronSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CronSetting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\CronSetting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CronSetting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CronSetting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CronSetting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\CronSetting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CronSetting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CronSettingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('cron_settings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('cron_name')
            ->maxLength('cron_name', 255)
            ->requirePresence('cron_name', 'create')
            ->notEmptyString('cron_name');

        $validator
            ->boolean('enabled')
            ->notEmptyString('enabled');

        return $validator;
    }
}
