<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fbpagesharetimes Model
 *
 * @property \App\Model\Table\FbpagesTable&\Cake\ORM\Association\BelongsTo $Fbpages
 *
 * @method \App\Model\Entity\Fbpagesharetime newEmptyEntity()
 * @method \App\Model\Entity\Fbpagesharetime newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagesharetime[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagesharetime get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagesharetime[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagesharetime|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpagesharetime[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class FbpagesharetimesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('fbpagesharetimes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Fbpages', [
            'foreignKey' => 'fbpage_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    // public function validationDefault(Validator $validator): Validator
    // {
    //     $validator
    //         ->integer('id')
    //         ->allowEmptyString('id', null, 'create');

    //     $validator
    //         ->time('from_time')
    //         ->requirePresence('from_time', 'create')
    //         ->notEmptyTime('from_time');

    //     $validator
    //         ->time('to_time')
    //         ->requirePresence('to_time', 'create')
    //         ->notEmptyTime('to_time');

    //     return $validator;
    // }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules): RulesChecker
    // {
    //     $rules->add($rules->existsIn(['fbpage_id'], 'Fbpages'), ['errorField' => 'fbpage_id']);

    //     return $rules;
    // }
}
