<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fbpages Model
 *
 * @property \App\Model\Table\ArticleprocessqueuesTable&\Cake\ORM\Association\HasMany $Articleprocessqueues
 *
 * @method \App\Model\Entity\Fbpage newEmptyEntity()
 * @method \App\Model\Entity\Fbpage newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Fbpage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fbpage get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fbpage findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Fbpage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fbpage[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fbpage|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fbpage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fbpage[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpage[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpage[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpage[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FbpagesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('fbpages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Fbpageposts', [
            'foreignKey' => 'fbpage_id',
            'saveStrategy' => 'replace'
        ])
        ->setDependent(true);

        $this->hasMany('Fbpagekeywords', [
            'foreignKey' => 'fbpage_id',
            'saveStrategy' => 'replace'
        ])
        ->setDependent(true);
        

        $this->hasMany('Fbpagesharetimes', [
            'foreignKey' => 'fbpage_id',
            'saveStrategy' => 'replace',
        ])
        ->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('page_title')
            ->maxLength('page_title', 255)
            ->requirePresence('page_title', 'create')
            ->notEmptyString('page_title');
            
        $validator
            ->scalar('fb_page_group')
            ->requirePresence('fb_page_group', 'create')
            ->notEmptyString('fb_page_group');

        $validator
            ->scalar('page_url')
            ->maxLength('page_url', 255)
            ->requirePresence('page_url', 'create')
            ->notEmptyString('page_url');

        $validator
            ->scalar('pageid')
            ->maxLength('pageid', 255)
            ->requirePresence('pageid', 'create')
            ->notEmptyString('pageid');

        // $validator
        //     ->scalar('fb_access_token')
        //     ->requirePresence('fb_access_token', 'create')
        //     ->notEmptyString('fb_access_token');

        return $validator;
    }
}
