<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Crawlers Model
 *
 * @method \App\Model\Entity\Crawler newEmptyEntity()
 * @method \App\Model\Entity\Crawler newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Crawler[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Crawler get($primaryKey, $options = [])
 * @method \App\Model\Entity\Crawler findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Crawler patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Crawler[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Crawler|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Crawler saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Crawler[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Crawler[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Crawler[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Crawler[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CrawlersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('crawlers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('feed_url')
            ->maxLength('feed_url', 255)
            ->requirePresence('feed_url', 'create')
            ->notEmptyString('feed_url');

        // $validator
        //     ->scalar('poll_frequency')
        //     ->maxLength('poll_frequency', 255)
        //     ->requirePresence('poll_frequency', 'create')
        //     ->notEmptyString('poll_frequency');

        return $validator;
    }
}
