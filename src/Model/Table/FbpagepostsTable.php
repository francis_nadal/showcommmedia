<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fbpageposts Model
 *
 * @property \App\Model\Table\FbpagesTable&\Cake\ORM\Association\BelongsTo $Fbpages
 * @property \App\Model\Table\KeywordsTable&\Cake\ORM\Association\BelongsTo $Keywords
 *
 * @method \App\Model\Entity\Fbpagepost newEmptyEntity()
 * @method \App\Model\Entity\Fbpagepost newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagepost[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagepost get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fbpagepost findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Fbpagepost patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagepost[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fbpagepost|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fbpagepost saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fbpagepost[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpagepost[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpagepost[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Fbpagepost[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class FbpagepostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('fbpageposts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER',
            'saveStrategy' => 'replace'
        ]);
        $this->belongsTo('Fbpages', [
            'foreignKey' => 'fbpage_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Keywords', [
            'foreignKey' => 'keyword_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    // public function validationDefault(Validator $validator): Validator
    // {
    //     $validator
    //         ->integer('id')
    //         ->allowEmptyString('id', null, 'create');

    //     $validator
    //         ->integer('status')
    //         ->requirePresence('status', 'create')
    //         ->notEmptyString('status');

    //     return $validator;
    // }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['fbpage_id'], 'Fbpages'), ['errorField' => 'fbpage_id']);
        $rules->add($rules->existsIn(['keyword_id'], 'Keywords'), ['errorField' => 'keyword_id']);
        $rules->add($rules->existsIn(['article_id'], 'Articles'), ['errorField' => 'article_id']);

        return $rules;
    }
}
