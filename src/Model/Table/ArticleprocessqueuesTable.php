<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articleprocessqueues Model
 *
 * @property \App\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsTo $Articles
 * @property \App\Model\Table\FbpagesTable&\Cake\ORM\Association\BelongsTo $Fbpages
 *
 * @method \App\Model\Entity\Articleprocessqueue newEmptyEntity()
 * @method \App\Model\Entity\Articleprocessqueue newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Articleprocessqueue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Articleprocessqueue get($primaryKey, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Articleprocessqueue[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Articleprocessqueue|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Articleprocessqueue[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArticleprocessqueuesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articleprocessqueues');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Fbpages', [
            'foreignKey' => 'fbpage_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    // public function validationDefault(Validator $validator): Validator
    // {
    //     $validator
    //         ->integer('id')
    //         ->allowEmptyString('id', null, 'create');

    //     $validator
    //         ->boolean('published')
    //         ->notEmptyString('published');

    //     $validator
    //         ->scalar('graphnode')
    //         ->maxLength('graphnode', 255)
    //         ->requirePresence('graphnode', 'create')
    //         ->notEmptyString('graphnode');

    //     return $validator;
    // }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['article_id'], 'Articles'), ['errorField' => 'article_id']);
        $rules->add($rules->existsIn(['fbpage_id'], 'Fbpages'), ['errorField' => 'fbpage_id']);

        return $rules;
    }
}
