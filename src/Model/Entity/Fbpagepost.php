<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fbpagepost Entity
 *
 * @property int $id
 * @property int $fbpage_id
 * @property int $keyword_id
 * @property int $status
 *
 * @property \App\Model\Entity\Fbpage $fbpage
 * @property \App\Model\Entity\Keyword $keyword
 */
class Fbpagepost extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_id' => true,
        'fbpage_id' => true,
        'keyword_id' => true,
        'articles' => true,
        'fbpage' => true,
        'keyword' => true,
    ];
}
