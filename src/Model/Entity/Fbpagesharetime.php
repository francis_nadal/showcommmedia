<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fbpagesharetime Entity
 *
 * @property int $id
 * @property int $fbpage_id
 * @property \Cake\I18n\Time $from_time
 * @property \Cake\I18n\Time $to_time
 *
 * @property \App\Model\Entity\Fbpage $fbpage
 */
class Fbpagesharetime extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fbpage_id' => true,
        'from_time' => true,
        'to_time' => true,
        'fbpage' => true,
    ];
}
