<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fbpage Entity
 *
 * @property int $id
 * @property string $page_title
 * @property string $page_url
 * @property string $pageid
 * @property string $fb_access_token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Articleprocessqueue[] $articleprocessqueues
 */
class Fbpage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'page_title' => true,
        'fb_page_group' => true,
        'page_url' => true,
        'pageid' => true,
        'fb_access_token' => true,
        'post_interval' => true,
        'page_group_assoc_id' => true,
        'created' => true,
        'modified' => true,
        'articleprocessqueues' => true,
        'fbpagekeywords' => true,
        'fbpagesharetimes' => true,
    ];
}
