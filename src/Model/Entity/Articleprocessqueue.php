<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Articleprocessqueue Entity
 *
 * @property int $id
 * @property int $article_id
 * @property int $fbpage_id
 * @property string $fb_caption
 * @property string $graphnode
 *
 * @property \App\Model\Entity\Article $article
 * @property \App\Model\Entity\Fbpage $fbpage
 */
class Articleprocessqueue extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_id' => true,
        'fbpage_id' => true,
        'fb_caption' => true,
        'shared_status' => true,
        'error_message' => true,
        'article' => true,
        'fbpage' => true,
    ];
}
