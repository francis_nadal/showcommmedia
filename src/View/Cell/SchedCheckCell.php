<?php
declare(strict_types=1);

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\I18n\FrozenTime;

class SchedCheckCell extends Cell
{
   
    protected $_validCellOptions = [];    
    public function initialize(): void
    {
        
    }

    public function display($fbpage_id)
    {
      $result = "";
      $this->loadModel('Fbpages');

      $fbpage = $this->Fbpages->findById($fbpage_id)
                              ->contain('Fbpagesharetimes');

      $now = FrozenTime::now();
      $time_now = $now->i18nFormat('HH:mm:ss');
      // echo "time now: " . $time_now . "<br>";

      foreach($fbpage as $fbpage_prop) {
          if(!empty($fbpage_prop->fbpagesharetimes)) {
              foreach($fbpage_prop->fbpagesharetimes as $fbpage_post_time) {
                $from_time = $fbpage_post_time->from_time;
                $to_time = $fbpage_post_time->to_time;
                
                if($time_now >= $from_time && $time_now <= $to_time) {
                    $result = "true";
                    break;
                } else {
                    $result = "false";
                }
              }
          } else {
              $result = "false";
          }
      }
      $this->set(compact('result'));
    }
}