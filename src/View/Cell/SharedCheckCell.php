<?php
declare(strict_types=1);

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\I18n\FrozenTime;

class SharedCheckCell extends Cell
{
   
    protected $_validCellOptions = [];    
    public function initialize(): void
    {
        
    }

    public function display($article_id, $fbpage_id )
    {
      // $result = "";
      $this->loadModel('SharedArticles');

      $shared_articles = $this->SharedArticles->find('all');
      
      if(isset($shared_articles)) {
        foreach($shared_articles as $shared_article) {
          if($shared_article->article_id == $article_id && $shared_article->fbpage_id == $fbpage_id) {
            $result = $shared_article;
            break;
          } else {
            $result = "null";
          }
        }
      }

      $this->set(compact('result'));
      
    }
}