<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Showcommmedia';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->css('custom-css') ?>
    <?= $this->Html->script('https://code.jquery.com/jquery-3.6.0.min.js', ['integrity' => 'sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=', 'crossorigin' => 'anonymous']) ?>
    <?= $this->Html->script('custom-js') ?>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    
</head>
<body onload="startTime()">
    <nav class="top-nav">
        <div class="top-nav-title">
            <a href="<?= $this->Url->build('/') ?>">Showcommmedia</a>
        </div>
        <div class="top-nav-links">
            <?php if($this->Identity->isLoggedIn()) { ?>
                <?= $this->Html->link(__('Crawler'), array('controller'=>'crawlers','action' => 'index')) ?>
                <?= $this->Html->link(__('Raw Articles'), array('controller'=>'articles','action' => 'raw')) ?>
                <?= $this->Html->link(__('Unused Articles'), array('controller'=>'articles','action' => 'unused')) ?>
                <?= $this->Html->link(__('Queued Articles'), array('controller'=>'articles','action' => 'index')) ?>
                <?= $this->Html->link(__('Published Articles'), array('controller'=>'articles','action' => 'published')) ?>
                <?= $this->Html->link(__('Keywords'), array('controller'=>'keywords','action' => 'index')) ?>
                <?= $this->Html->link(__('Facebook Groups/Pages'), array('controller'=>'fbpages','action' => 'index')) ?>
                <?php //$this->Html->link(__('Scheduler'), array('controller'=>'schedulers','action' => 'index')) ?>
                <?php //$this->Html->link(__('Stats Posting'), array('controller'=>'statpostings','action' => 'index')) ?>
            <?php } ?>
        </div>
        <div class="user-logged-in">
            <?php if($this->Identity->isLoggedIn()) { ?>
                Hello, <?php echo $loggedInUsername . ' ';
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fas fa-edit')),
                    ['controller' => 'Users', 'action' => 'edit', $this->Identity->get('id')],
                    ['escape' => false]
                  );
            } ?>  
        </div>
        <div class="time-now">Time Now: <div id="txt"></div></div>
        <div class="user-log-out">
            <?php if($this->Identity->isLoggedIn()) { ?>
                <?= $this->Html->link(__('Logout'), array('controller'=>'access','action' => 'logout'), ['class' => 'red-button']) ?>
            <?php } ?> 
        </div>
    </nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
        <div class="copyright">&copy; 2021 All Right Reserved.</div>
        <div class="privacy-policy"><?= $this->Html->link(__('Privacy Policy'), array('controller'=>'privacy','action' => 'index')) ?></div>
        <div class="by-gemango">Site by <a href="https://www.gemango.com" _target="blank">Gemango</a></div>
    </footer>

    <script>
    function startTime() {
        const today = new Date(new Date().toLocaleString("en-US", {timeZone: "Europe/Brussels"}));
        let h = today.getHours();
        let m = today.getMinutes();
        let s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('txt').innerHTML =  h + ":" + m + ":" + s;
        setTimeout(startTime, 1000);
      }
      
      function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
      }
</script>
</body>
</html>

