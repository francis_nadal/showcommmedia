<h1>Schedulers</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($schedulers as $scheduler): ?>
    <tr>
        <td>
          <?= $scheduler->id; ?>
        </td>
        <td>
          <?= $this->Html->link($scheduler->name, ['action' => 'view', $scheduler->id]) ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>