<h1><?= h($crawler->name) ?></h1>
<p>Feed URL: <a href="<?= $crawler->feed_url ?>" target="_blank" ><?= $crawler->feed_url ?></a></p>
<p>Polling Frequency: <?= $crawler->poll_frequency ?></p>
<p>
    <?= $this->Form->postLink(
                    'Override: Manually Initiate XML Feed Crawling',
                    ['action' => 'crawl', $crawler->id],
                    ['class '=> 'override-button'],
                    ['confirm' => 'Are you sure?'])
    ?>
</p>