<h1>Add Feed</h1>

<?php

  echo $this->Form->create($crawler);
  echo $this->Form->control('name');
  echo $this->Form->control('feed_url');
  // echo '<label class="crawler-poll-frequency-label">Choose Poll Frequency:</label>';
  // echo $this->Form->select(
  //   'poll_frequency',
  //   [
  //     'Every hour' => 'Every hour',
  //     'Every 5 hours' => 'Every 5 hours', 
  //     'Once a day' => 'Once a day'
  //   ],
  //   ['empty' => '(choose one)']
  // );
  echo $this->Form->button(__('Add Feed'));
  echo $this->Form->end();

?>