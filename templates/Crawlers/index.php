<h1>Crawlers</h1>
  <div class="action-btn-container">
    <?= $this->Html->link('Add Feed', ['action' => 'add'],['class '=> 'button']) ?>
    <div class="cron-buttons">
      <?php
        if($cron_status == true) {
          echo $this->Form->postLink(
            $this->Html->tag('i', '', array('class' => 'fas fa-toggle-on')) . " Pause XML Feed Crawling CRON",
            ['controller' => 'Crawlers', 'action' => 'disableFeedCrawlerCron'], ['escape' => false]
            // ['class '=> 'override-button']
          );
        } else {
          echo $this->Form->postLink(
            $this->Html->tag('i', '', array('class' => 'fas fa-toggle-off')) . " Initiate XML Feed Crawling CRON",
            ['controller' => 'Crawlers', 'action' => 'enableFeedCrawlerCron'], ['escape' => false]
            // ['class '=> 'override-button']
          );
        }
      ?>
    </div>
    <?php /*$this->Html->link(
                    'Override: Manually Initiate XML Feed Crawling',
                    ['action' => 'crawl'],
                    ['class '=> 'override-button'])*/
    ?>
  </div>
<table class="index">
    <tr>
        <th>Name</th>
        <th>Feed URL</th>
        <th>Actions</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($crawlers as $crawler): ?>
    <tr>
        <td>
          <?= $this->Html->link($crawler->name, ['action' => 'view', $crawler->id]) ?>
        </td>
        <td>
          <a href="<?= $crawler->feed_url ?>" target="_blank" ><?= $crawler->feed_url ?></a>
        </td>
        <td>
          <?= $this->Html->link('Crawl Feed',
                  ['action' => 'crawl', $crawler->id],
                  ['class '=> 'override-button'],
                  ['confirm' => 'Are you sure?']) 
          ?>
          
          <?= $this->Html->link('Edit', ['action' => 'edit', $crawler->id], ['class' => 'red-button']) ?>
          <?= $this->Form->postLink(
                  'Delete',
                  ['action' => 'delete', $crawler->id],
                  ['confirm' => 'Are you sure?'])
          ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>