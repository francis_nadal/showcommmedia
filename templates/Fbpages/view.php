<div class="fb-pages-view">
  <div div class="edit-section">
    <?= $this->Html->link('Edit', ['action' => 'edit', $fbpage->id], ['class' => 'red-button']) ?>
  </div>
</div>

<div class="fb-page-details-panel">
  <div class="fb-page-name">
    <h1>
      <?= h($fbpage->page_title) . " (". $fbpage->fb_page_group . ")" ?>
      <?php echo (isset($fbpage->link_group_name)) ? ' - Connected to ' . $fbpage->link_group_name . ' (' . $fbpage->link_group_type . ')' : ''; ?>
    </h1>
    <a href="<?= $fbpage->page_url; ?>" target="_blank"><?= $fbpage->page_url; ?></a>
    <div class="fb-page-sharing-interval-panel">
      <h5>Sharing Interval: </h5> 
        <?php 
          if(isset($fbpage->post_interval)) {
            if($fbpage->post_interval > 30) {
              echo $fbpage->post_interval / 60 . " hour";
            } else {
              echo $fbpage->post_interval . " minutes";
            }
          }
        ?>
    </div>
  </div>
  <!-- <div class="fb-page-details">
    <div class="fb-page-poster">
      <p>Poster ID/User: Account Test 1</p>
    </div>
    <div class="fb-page-license">
      <p>Length of License: 37d3125c3dc065a58c66f1053f199d9f</p>
    </div>
  </div> -->
  <div class="keyword-posting-time-panel">
    <div class="fb-page-keywords-panel">
      <h5>Keywords Found:</h5>
        <?php if(!empty($sorted_keywords)) { ?>
          <ul class="fb-page-keywords-list">
            <?php foreach($sorted_keywords as $keyword): ?>
              <li><?php echo $keyword ?></li>
            <?php endforeach; ?>
          </ul>
          <?php } else { ?>
            <h5>No Keywords assigned for this page.</h5>
          <?php } ?>
    </div>

    <div class="fb-page-sharing-panel">
      <div class="fb-page-posting-time-panel">
        <h5>Posting Times:</h5>
        <ul class="fb-page-posting-time-list">
          <?php 
            if($fbpage_sharetimes->count()) {
              foreach($fbpage_sharetimes as $fbpage_sharetime):
                echo '<li>' . $fbpage_sharetime->from_time . ' - ' . $fbpage_sharetime->to_time . '</li>';
              endforeach;
            } else{
              echo '<h5>No Posting Times for this page.</h5>';
            }
            // foreach($fbpage_sharetimes as $fbpage_sharetime): 
            //   // if(!isset($fbpage_sharetime->id)) { 
            //     // echo "huh";
            //     // echo '<h5>No Posting Times for this page check id.</h5>';
            //   // } else {
            //     echo "huh <br>";
            //     echo $fbpage_sharetime->id;
            //     echo '<li>' . $fbpage_sharetime->from_time . ' - ' . $fbpage_sharetime->to_time . '</li>';
            //   // }
            // endforeach;
          ?>
        </ul>
      </div>
    </div>    
  </div>

  <div class="articles-posted-today-panel">
    <h5>Articles Posted Today:</h5>
    <table>
      <thead>
        <th>No.</th>
        <th>Title</th>
        <th>Graph Node ID</th>
        <th>Shared Time</th>
      </thead>
      <tbody>
        <?php
          $count = 1;
          foreach ($articles_today as $article): ?>
          <tr>
            <td>
              <?= $count++; ?>
            </td>
            <td><?= $this->Html->link(__($article['article']['title']), ['controller' => 'Articles', 'action' => 'view', $article['article']['id']]); ?></td>
            <td><?= $article['graph_node_id']; ?></td>
            <td><?= $article['created']->i18nFormat('d/M/YYYY - hh:mm:ss a'); ?></td>
          </tr>

          <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  
</div>

