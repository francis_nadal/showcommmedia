<h1>Facebook Groups/Pages</h1>
  <?= $this->Html->link('Add Facebook Group/Page', ['action' => 'add'],['class '=> 'button']) ?>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
<table class="index">
    <tr>
        <th>Page Name</th>
        <th>Type</th>
        <th>Connected to Group</th>
        <th>Group/Page URL</th>
        <th style="text-align:center;">Assigned Keywords</th>
        <th>Actions</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($fbpages as $fbpage): ?>
    <tr>
        <td>
          <?= $this->Html->link($fbpage->page_title, ['action' => 'view', $fbpage->id]) ?>
        </td>
        <td>
          <?= $fbpage->fb_page_group ?>
        </td>
        <td>
          <?php
            foreach($fbpage_groups as $fb_group):
              if($fbpage->page_group_assoc_id == $fb_group->id)
                echo $this->Html->link($fb_group->page_title, ['action' => 'view', $fb_group->id]);
            endforeach;
          ?>
        </td>
        <td>
          <a href="<?= $fbpage->page_url ?>" target="_blank" ><?= $fbpage->page_url ?></a>
        </td>
        <td style="text-align:center;">
          <?php
            if(isset($fbpage->fbpagekeywords)) {
              // foreach($keyword->fbpagekeywords as $fbpagekeyword) {
                echo count($fbpage->fbpagekeywords);
              // }
            }
          ?>
        </td>
        <td>
          <?= $this->Html->link('Edit', ['action' => 'edit', $fbpage->id], ['class' => 'red-button']) ?>
          <?= $this->Form->postLink(
              'Delete',
              ['action' => 'delete', $fbpage->id],
              ['confirm' => 'Are you sure?'])
          ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<div class="paginator">
  <ul class="pagination">
    <?= $this->Paginator->first('<< ' . __('first')) ?>
    <?= $this->Paginator->prev('< ' . __('previous')) ?>
    <?= $this->Paginator->numbers() ?>
    <?= $this->Paginator->next(__('next') . ' >') ?>
    <?= $this->Paginator->last(__('last') . ' >>') ?>
  </ul>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
</div>