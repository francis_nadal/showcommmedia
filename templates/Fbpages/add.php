<h1>Add FB Group/Page</h1>

<?php

  echo $this->Form->create($fbpage, ['id' => 'fbpage-form']);
  echo $this->Form->control('page_title', ['label' => 'Facebook Group/Page Title']);
  echo $this->Form->control('page_url',  ['label' => 'Facebook Group/Page URL']);
  echo $this->Form->control('pageid',  ['id' => 'pageid', 'label' => 'Facebook Group/Page ID']);
  echo $this->Form->radio(
    'fb_page_group',
    [
        ['value' => 'Page', 'text' => 'Page', 'label' => ['class' => 'page-option']],
        ['value' => 'Group', 'text' => 'Group', 'label' => ['class' => 'group-option']],
    ]
  );
  echo '<div class="more-options">';
  echo '<label id="fb_access_token_label" for="fb_caption">Facebook Access Token</label> <button id="btn-generate-token">Generate Page Token</button>' . $this->Form->textarea('fb_access_token', ['id' => 'fb_access_token', 'rows' => '15', 'cols' => '5']);
  echo '<label class="article-fb-caption">Choose which Keywords to Associate This Page:</label>';
  echo '<button id="selectAllKeywords" style="display:inline-block">Select All</button>';
  echo '<div class="fbage-keyword-container">';
  echo '<ul class="keyword-column">';
  foreach($keywords as $index => $keyword) :
    echo '<li><label for="showcomm_checkbox_'.$index.'" class="fb-keyword-option">' . $this->Form->checkbox('fbpagekeywords.'.$index.'.keyword_id', ['value' => $keyword['id'], 'id' => 'showcomm_checkbox_'.$index.'','class' => 'keyword-checkbox', 'hiddenField' => false]) . $keyword['keyword'] . '</label></li>';
  endforeach;
  echo '</ul>';
  echo '</div>';
  echo '</div>';  
  echo '<label class="crawler-poll-frequency-label">Choose Post Interval:</label>';
  echo $this->Form->select(
    'post_interval',
    [
      '15' => '15 minutes',
      '30' => '30 minutes',
      '60' => '1 hour',
      '120' => '2 hours',
      '180' => '3 hours',
      '240' => '4 hours',
      '300' => '5 hours',
      '360' => '6 hours',
      '420' => '7 hours',
      '480' => '8 hours',
      '540' => '9 hours',
      '600' => '10 hours'
    ],
    ['empty' => '(choose one)']
  );
  echo '<div class="more-options">';
  echo '<label class="article-fb-caption">Choose Time to Post Articles:</label>';
  echo '<div class="sched-time-panel">';
  echo '<div class="sched-time-fields">';
  echo '<div class="sched-time-row">';
  // echo $this->form->input('fbpagesharetimes.0.from_time', ['label' => false, 'id' => 'sched-wrap']);
  // echo $this->form->input('fbpagesharetimes.0.to_time', ['label' => false, 'id' => 'sched-wrap']);
  // echo $this->Html->link('Remove Schedule', '#btn-remove-time', ['id' => 'btn-remove-time-0', 'class' => 'remove-me red-button']);
  echo '</div>';
  echo '</div>';
  echo $this->Html->link('Add Schedule', '#btn-add-time', ['id' => 'btn-add-time']);
  echo '</div>';
  echo '</div>';
  
  echo '<span class="create-btn">' . $this->Form->button(__('Add')) . '</span>';
  echo $this->Form->end();
?>

<div id="template-inputs" style="display:none;">
	<?php 
    echo '<div class="sched-time-row">';
    echo $this->Form->time('fbpagesharetimes.::num.from_time', ['label' => false, 'id' => 'sched-wrap-::num']);
    echo $this->Form->time('fbpagesharetimes.::num.to_time', ['label' => false, 'id' => 'sched-wrap-::num']);
    echo $this->Html->link('Remove Schedule', '#btn-remove-time', ['id' => 'btn-remove-time-::num', 'class' => 'remove-me red-button']);
    echo '</div>';
  ?>
</div>