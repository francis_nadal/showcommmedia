<h1>Edit FB <?php echo $fbpage->fb_page_group; ?></h1>

<?php

  echo $this->Form->create($fbpage, ['id' => 'fbpage-form']);
  echo $this->Form->control('page_title', ['label' => 'Facebook Group/Page Title']);
  echo $this->Form->control('page_url',  ['label' => 'Facebook Group/Page URL']);
  echo $this->Form->control('pageid',  ['label' => 'Facebook Group/Page ID']);

  echo '<div '.(($fbpage->fb_page_group == 'Group') ? 'style="display: none;"' : '').'>';
  echo '<label class="crawler-poll-frequency-label">Page Group:</label>';
  // echo $this->Form->control('page_group_assoc_id',array('type' => 'select','options' => $fbpage_group));
  if(isset($fbpage_group_options)) {
    echo $this->Form->select(
      'page_group_assoc_id',
      $fbpage_group_options,
      ['empty' => '(choose one)']
    );
  }
  echo '</div>';

  // echo $this->Form->radio(
  //   'fb_page_group',
  //   [
  //       ['value' => 'Page', 'text' => 'Page', 'label' => ['class' => 'page-option']],
  //       ['value' => 'Group', 'text' => 'Group', 'label' => ['class' => 'group-option']],
  //   ]
  // );

  if($fbpage->fb_page_group == 'Page') {
    $readonly = 0;
    echo '<label id="fb_access_token_label" for="fb_caption">Facebook Access Token</label> <button id="btn-generate-token">Generate Page Token</button>' . $this->Form->textarea('fb_access_token', ['id' => 'fb_access_token', 'rows' => '15', 'cols' => '5']);
  } else {
    $readonly = 1;
    echo '<label style="display: none;" id="fb_access_token_label" for="fb_caption">Facebook Access Token</label> <button style="display: none;" id="btn-generate-token">Generate Page Token</button>' . $this->Form->textarea('fb_access_token', ['id' => 'fb_access_token', 'style' => 'display: none;', 'readonly' => 'readonly', 'rows' => '15', 'cols' => '5']);
  }
  echo '<label class="article-fb-caption">Assign keywords to Facebook Page:</label>';
  echo '<button id="selectAllKeywords" style="display:inline-block">Select All</button>';
  echo '<div class="fbage-keyword-container" '. ($readonly ? 'style="pointer-events: none;' : '') .' ">';
  echo '<ul class="keyword-column">';
?>

<?php
  foreach($keywords as $index => $keyword) :
    $checked = false;
    foreach($fbpage->fbpagekeywords as $page_keyword_item) {
      if($page_keyword_item['keyword_id'] === $keyword['id']) {
        $checked = true;
      } 
    }
?>

<?php

  echo '<li><label for="showcomm_checkbox_'.$index.'" class="fb-keyword-option">' 
        . $this->Form->checkbox( 'fbpagekeywords.'.$index.'.keyword_id',
          ['value' => $keyword['id'],
           'hiddenField' => false,
           'checked' => $checked,
           'id' => 'showcomm_checkbox_'.$index.'',
           'class' => 'keyword-checkbox',
          ]) 
        . $keyword->keyword . '</label></li>';
  
  endforeach;
  echo '</ul>';
  echo "</div>";

  echo '<div class="post-interval-container">';
  echo '<label class="crawler-poll-frequency-label">Choose Post Interval:</label>';
  echo $this->Form->select(
    'post_interval',
    [
      '15' => '15 minutes',
      '30' => '30 minutes',
      '60' => '1 hour',
      '120' => '2 hours',
      '180' => '3 hours',
      '240' => '4 hours',
      '300' => '5 hours',
      '360' => '6 hours',
      '420' => '7 hours',
      '480' => '8 hours',
      '540' => '9 hours',
      '600' => '10 hours'
    ],
    ['empty' => '(choose one)']
  );

  echo '<label class="article-fb-caption">Choose Time to Post Articles:</label>';
  echo "</div>";
  echo '<div class="sched-time-panel" '. ($readonly ? 'style="pointer-events: none;' : '') .'>';
  echo '<div class="sched-time-fields">';
?>

<?php

  foreach($fbpage_sharetimes as $index => $share_time) {
    if($share_time->fbpage_id == $fbpage->id) {
      echo '<div class="sched-time-row">';
      // echo "test";
      echo $this->Form->time('fbpagesharetimes.'.$index.'.from_time', ['label' => false, 'id' => 'sched-wrap-'.$index.'', 'value' => $share_time->from_time]);
      echo $this->Form->time('fbpagesharetimes.'.$index.'.to_time', ['label' => false, 'id' => 'sched-wrap-'.$index.'', 'value' => $share_time->to_time]);
      echo $this->Html->link('Remove Schedule', '#btn-remove-time', ['id' => 'btn-remove-time-'.$index.'', 'class' => 'remove-me red-button']);
      echo '</div>';
    }
    
  }
?>

<?php
  echo '</div>';
  echo $this->Html->link('Add Schedule', '#btn-add-time', ['id' => 'btn-add-time']);
  echo '</div>';
  
  echo '<span class="create-btn">' . $this->Form->button(__('Update')) . '</span>';
  echo $this->Form->end();

?>

<div id="template-inputs" style="display:none;">
	<?php 
    // echo $fbpage->id."<br>";
    echo '<div class="sched-time-row">';
    echo $this->Form->time('fbpagesharetimes.::num.from_time', ['label' => false, 'id' => 'sched-wrap-::num']);
    echo $this->Form->time('fbpagesharetimes.::num.to_time', ['label' => false, 'id' => 'sched-wrap-::num']);
    echo $this->Html->link('Remove Schedule', '#btn-remove-time', ['id' => 'btn-remove-time-::num', 'class' => 'remove-me red-button']);
    echo '</div>';
  ?>
</div>