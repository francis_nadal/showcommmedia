<h1>Published Articles</h1>
<div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>

  <?php
    $page = $this->Paginator->counter('{{page}}');
    $limit = 100; 
    $item_counter = ($page * $limit) - $limit + 1;
  ?>

<table class="index" id="published-table">
    <tr>
        <th>No.</th>
        <th>Article Title</th>
        <th>Shared To Facebook Group/Page</th>
        <th>Graph Node ID</th>
        <th>Shared Date</th>
        <!-- <th colspan="2">Actions</th> -->
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php
      $count = 1;
      foreach ($published_articles as $article) { ?>
    <tr>
        <td>
          <?php echo $item_counter++ ?>
        </td>
        <td>
          <?php echo $this->Html->link($article->article->title, ['action' => 'view', $article->article->id]) ?>
        </td>
        <td>
          <?php echo '<a href="'.$article->fbpage->page_url.'" class="fb-page-link" target="_blank"> ('. $article->fbpage->fb_page_group . ') </a>'.$this->Html->link(__($article->fbpage->page_title), ['controller' => 'Fbpages', 'action' => 'view', $article->fbpage->id]); ?>
        </td>
        <td>
          <?php echo $article->graph_node_id; ?>
        </td>
        <td>
          <?php echo $article->created->i18nFormat('d/M/YYYY - hh:mm:ss a'); ?>
        </td>
    </tr>
<?php    }
          
      // } ?>
  <?php //} ?>
</table>

<div class="paginator">
  <ul class="pagination">
      <?= $this->Paginator->first('<< ' . __('first')) ?>
      <?= $this->Paginator->prev('< ' . __('previous')) ?>
      <?= $this->Paginator->numbers() ?>
      <?= $this->Paginator->next(__('next') . ' >') ?>
      <?= $this->Paginator->last(__('last') . ' >>') ?>
  </ul>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
</div>