<h1>Edit Article</h1>

<?php
  
  echo '<div class="article-edit-form">';
  echo $this->Form->create($article);
  echo $this->Form->control('title');
  echo $this->Form->control('url', ['label' => 'Article URL']);
  echo '<label for="fb_caption">Facebook Feed Caption</label>' . $this->Form->textarea('fb_caption', ['rows' => '15', 'cols' => '5']);
  echo '<label class="article-fb-caption" style="display:inline-block;margin-right:30px;margin-bottom: 50px;">Choose which Pages to Posts:</label> <button id="unselect-all" style="display:inline-block">Unselect All</button>';
  echo '<div class="article-fb-selection">';
  echo '<ul class="fbpages-column">';
?>

<?php

  foreach($fbpages as $index => $fbpage) :
    if($fbpage['fb_page_group'] == 'Page' && $fbpage['status'] == 1) {
      $checked = false;
      $shared = "";
      $disabled = "";
      $published = 0;
      $error_msg_value = NULL;
      
      foreach($article->articleprocessqueues as $article_page_item) {
        
        if($article_page_item['fbpage_id'] === $fbpage['id']) {
          $checked = true;
        }
        
        if($article_page_item['fbpage_id'] === $fbpage['id'] && $article_page_item['shared_status'] == true) {
          $shared = '<i class="fas fa-info-circle"></i>';
          $disabled = "disabled";
          $published = $article_page_item['shared_status'];
        }

        if($article_page_item['fbpage_id'] === $fbpage['id'] && $article_page_item['error_message'] != NULL) {
          $shared = '<i class="fas fa-info-circle"></i>';
          $error_msg_value = $article_page_item['error_message'];
        }
      }
?>

<?php
  echo '<li>
          <label for="showcomm_checkbox_'.$index.'" class="article-fb-option" id="fb_selection_'.$index.'">' 
            . $this->Form->checkbox( 'articleprocessqueues.'.$index.'.fbpage_id',
              [
                'value' => $fbpage['id'],
                'id' => 'showcomm_checkbox_'.$index.'',
                'hiddenField' => false,
                'checked' => $checked,
              ]) 
            . $fbpage['page_title'] . ' (' . $fbpage['fb_page_group'] . ') ' .

              $this->Form->hidden('articleprocessqueues.'.$index.'.shared_status', 
              [
                'id' => 'publish_status_' . $index,
                'value' => $published,
                'disabled' => ($published == 1 ? false : true)
              ]) .

              $this->Form->hidden('articleprocessqueues.'.$index.'.error_message', 
              [
                'class' => 'error-message-textfield',
                'id' => 'error_message_' . $index,
                'value' => $error_msg_value,
                'disabled' => ($error_msg_value != NULL ? false : true)
              ])
            .
          '</label>';

          if($published == 1) {
            echo ' <span id="shared-icon-info">'. $shared . '<span id="shared-tooltip">This article has been shared to this Page/Group.</span></span>';
          }

          if($error_msg_value != NULL) {
            echo ' <span id="error-icon-info">'. $shared . '<span id="error-tooltip">Failed to Share to this Page/Group.</span>';
          }
          
  echo '</li>';
  }
  endforeach;
  
  echo '</ul>';
  echo '<span class="create-btn">' . $this->Form->button(__('Update')) . '</span>';
  echo $this->Form->end();
  echo '</div>';

?>