<h1>Add Article</h1>

<?php

  echo $this->Form->create($article);
  // echo $this->Form->control('title');
  echo $this->Form->control('url', ['label' => 'Article URL']);
  echo '<label for="fb_caption">Facebook Feed Caption</label>' . $this->Form->textarea('fb_caption', ['rows' => '15', 'cols' => '5']);
  echo '<label class="article-fb-caption">Choose which Pages to Posts:</label>';
  echo '<div class="article-fb-selection">';
  echo '<ul class="fbpages-column">';
  foreach($fbpages as $index => $fbpage) :
    if($fbpage['fb_page_group'] == 'Page' && $fbpage['status'] == 1) {
      // echo '<span class="article-fb-option">' . $this->Form->checkbox('articleprocessqueues.'.$index.'.fbpage_id', ['value' => $fbpage['id'], 'hiddenField' => false]) . $fbpage['page_title'] . '</span>';
    echo '<li><label for="showcomm_checkbox_'.$index.'" class="article-fb-option">' . $this->Form->checkbox('fbpageposts.'.$index.'.fbpage_id', ['value' => $fbpage['id'], 'id' => 'showcomm_checkbox_'.$index.'', 'hiddenField' => false]) . $fbpage['page_title'] . ' (' . $fbpage['fb_page_group'] . ')</label></li>';
    }    
  endforeach;
  echo '</ul>';
  echo '</div>';
  echo '<span class="create-btn">' . $this->Form->button(__('Add')) . '</span>';
  echo $this->Form->end();
?>