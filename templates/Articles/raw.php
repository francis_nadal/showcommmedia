<h1>Raw Articles</h1>
  <div class="action-btn-container">
    <?= $this->Html->link('Add Raw Articles', ['action' => 'addRaw'],['class '=> 'button']) ?>
    <div class="cron-buttons">
      <?php 
        // if(count($articles) > 0) {
          // echo $this->Html->link(
          //   'Initiate Keyword Matching',
          //   ['controller' => 'Articles', 'action' => 'cronCrawl'],
          //   ['class '=> 'override-button']
          // );
          if($cron_status == true) {
            echo $this->Form->postLink(
              $this->Html->tag('i', '', array('class' => 'fas fa-toggle-on')) . " Pause Keyword Matching CRON",
              ['controller' => 'Articles', 'action' => 'disableKeywordMatcherCron'], ['escape' => false]
              // ['class '=> 'override-button']
            );
          } else {
            echo $this->Form->postLink(
              $this->Html->tag('i', '', array('class' => 'fas fa-toggle-off')) . " Initiate Keyword Matching CRON",
              ['controller' => 'Articles', 'action' => 'enableKeywordMatcherCron'], ['escape' => false]
              // ['class '=> 'override-button']
            );
          }
          
        // }
      ?>
    </div>
  </div>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
<table class="index" id="raw-table">
    <tr>
        <th>No.</th>
        <th>Article Title</th>
        <th>Article URL</th>
        <th>Article Published</th>
        <th style="text-align:right;">Actions</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php
      $count = 1;
      foreach ($articles as $article): ?>
    <tr>
        <td>
          <?= $count++; ?>
        </td>
        <td>
          <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
        </td>
        <td>
          <a href="<?= $article->url ?>" target="_blank" ><?= $article->url ?></a>
        </td>
        <td>
          <?= $article->lastmod ? $article->lastmod->i18nFormat('d/M/YYYY -  hh:mm:ss a') : '' ?></a>
        </td>
        <td style="text-align:right;">
<?php
      if($article->article_type == 'raw') {
        $parse = parse_url($article->url);
        if($parse['host'] == 'www.showbizzsite.be') {
          echo $this->Html->link(
            'Match Keyword',
            ['action' => 'crawlArticle', $article->id],
            ['class' => 'override-button']
          );
        }

        if($parse['host'] == 'www.vipnieuws.be' || $parse['host'] == 'entertainment-today.be' || $parse['host'] == 'showbizznieuws247.be' || $parse['host'] == 'nieuws247.be' || $parse['host'] == 'vipnieuws.nl' || $parse['host'] == 'showbizz365.be' || $parse['host'] == 'entertainment-today.nl' || $parse['host'] == 'showbizz365.nl' || $parse['host'] == 'mudoo.be' || $parse['host'] == 'showbizznieuws247.nl' || $parse['host'] == 'showbizz247.nl'|| $parse['host'] == 'showbiss.nl' || $parse['host'] == 'show24.be' || $parse['host'] == 'show24.nl' || $parse['host'] == 'soapkijker.be' || $parse['host'] == 'royalty247.nl' || $parse['host'] == 'vandaag247.nl' || $parse['host'] == 'vandaag247.be' || $parse['host'] == 'happy247.be' || $parse['host'] == 'happy247.nl' || $parse['host'] == 'showcafe.be' || $parse['host'] == 'showcafe.nl' || $parse['host'] == 'redactie247.be' || $parse['host'] == 'redactie247.nl' || $parse['host'] == 'showboulevard.nl' || $parse['host'] == 'mudoo.nl' || $parse['host'] == 'nl.nieuws-247.be' || $parse['host'] == 'hbvv.be' || $parse['host'] == 'redactie365.be' || $parse['host'] == 'royalty247.be') {
          echo $this->Html->link(
            'Match Keyword',
            ['action' => 'wordpressCrawler', $article->id],
            ['class' => 'override-button']
          );
        }

        if($parse['host'] == 'www.msn.com') {
          echo $this->Html->link(
            'Match Keyword',
            ['action' => 'msnCrawler', $article->id],
            ['class' => 'override-button']
          );
        }

        if($parse['host'] == 'sport247.be') {
          echo $this->Html->link(
            'Match Keyword',
            ['action' => 'sport247Crawler', $article->id],
            ['class' => 'override-button']
          );
        }

        if($parse['host'] == 'www.showbizzsite.nl') {
          echo $this->Html->link(
            'Match Keyword',
            ['action' => 'showbizzsitenlCrawler', $article->id],
            ['class' => 'override-button']
          );
        }
      } ?>
        
          <?= $this->Html->link('Edit', ['action' => 'edit', $article->id], ['class' => 'red-button']) ?>
        
          <?= $this->Form->postLink(
                'Delete',
                ['action' => 'deleteArticle', $article->id],
                ['confirm' => 'Are you sure?']
                )
          ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <div class="table-info">
      <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>