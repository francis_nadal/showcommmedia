<h1>Unused Articles</h1>
<div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>

  <?php
    $page = $this->Paginator->counter('{{page}}');
    $limit = 50; 
    $item_counter = ($page * $limit) - $limit + 1;
  ?>

<table id="unused-table">
    <tr>
        <th>No.</th>
        <th>Article Title</th>
        <th>Article URL</th>
        <th>Article Published</th>
        <th style="text-align:right;">Actions</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php
        $count = 1; 
        foreach ($articles as $article): ?>
    <tr>
        <td>
          <?= $item_counter++; ?>
        </td>
        <td>
          <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
        </td>
        <td>
          <a href="<?= $article->url ?>" target="_blank" ><?= $article->url ?></a>
        </td>
        <td>
            <?= $article->lastmod->i18nFormat('d/M/YYYY -  hh:mm:ss a') ?></a>
        </td>
        <td style="text-align:right;">
            <?= $this->Html->link('Edit', ['action' => 'edit', $article->id], ['class' => 'red-button']) ?>
            <?= $this->Form->postLink(
                'Delete',
                ['action' => 'deleteArticle', $article->id],
                ['confirm' => 'Are you sure?']
                )
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <div class="table-info">
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>

<div style="text-align:center;">
    <?= $this->Form->postLink(
                    'Delete All Unused Articles',
                    ['action' => 'clearAllUnusedArticles'],
                    ['class '=> 'red-button'])
    ?>
</div>