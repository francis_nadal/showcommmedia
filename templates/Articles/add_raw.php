<h1>Add Raw Article</h1>

<?php

  echo $this->Form->create($article);
  // echo $this->Form->control('title');
  echo $this->Form->control('url', ['label' => 'Article URL']);
  echo '<label for="fb_caption">Facebook Feed Caption</label>' . $this->Form->textarea('fb_caption', ['rows' => '15', 'cols' => '5']);
  // echo '<label class="article-fb-caption">Choose which Pages to Posts:</label>';
  // echo '<div class="article-fb-selection">';
  // foreach($fbpages as $index => $fbpage) :
  //   echo '<span class="article-fb-option">' . $this->Form->checkbox('articleprocessqueues.'.$index.'.fbpage_id', ['value' => $fbpage['id'], 'hiddenField' => false]) . $fbpage['page_title'] . '</span>';
  // endforeach;
  // echo '</div>';
  echo '<span class="create-btn">' . $this->Form->button(__('Add')) . '</span>';
  echo $this->Form->end();
?>