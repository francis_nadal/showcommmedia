<?php
        if(isset($og_meta)) {
          $og_site_name = $og_meta["og:site_name"];
          $og_image = $og_meta["og:image"];
          $og_title = $og_meta["og:title"];
          $og_description = $og_meta["og:description"];
          // $og_site_name = $og_meta["og:site_name"];
        } else {
          $og_site_name = 'Unable to get information from site';
          $og_image = '//via.placeholder.com/498x280';
          $og_title = 'Unable to get information from site';
          $og_description = 'Unable to get information from site';
        }
      ?>
<div class="article-view">
  <div class="action-section">
    <?php 
    
      echo $this->Html->link(
                            'Edit',
                            ['action' => 'edit', $article->id],
                            ['class' => 'red-button']
      );

      echo $this->Form->postLink(
        'Delete',
        ['action' => 'delete', $article->id],
        ['confirm' => 'Are you sure?']
      );
    ?>
  </div>
  <!-- <div class="delete-section">
    
  </div> -->
  <div class="article-meta-panel">
    <div class="article-share-preview">
      <div class="share-container">
        <div class="og-img">
          <img src="<?= $og_image ?>" >
        </div>
        <div class="og-details">
          <span class="site-url"><a href="#"><?= $the_site ? $the_site : "Unable to get information from site" ?></a></span>
          <div class="title-and-desc">
            <span class="og-title"><a href="#"><?= $article->title ?></a></span>
            <span class="og-description"><a href="#"><?= $og_description ?></a></span>
          </div>
        </div>
      </div>
    </div>
    <div class="article-meta-info">
      
      <table>
        <tbody>
          <tr>
            <td>og:site_name</td>
            <td><a href="https://<?= $the_site ?>" target="_blank"><?= $og_site_name ?></a></td>
          </tr>
          <tr>
            <td>og:url</td>
            <td><a href="<?= $article->url ?>" target="_blank" ><?= $article->url ?></a></td>
          </tr>
          <tr>
          <td>og:image</td>
            <td><?= $og_image ?></td>
          </tr>
          <td>og:title</td>
            <td><?= $og_title ?></td>
          </tr>
          <td>og:description</td>
            <td><?= $og_description ?></td>
          </tr>
        </tbody>
      </table>
      <div class="article-facebook-caption">
        <h5>Facebook Feed Caption:</h5>
        <p><?= $article->fb_caption ? $article->fb_caption : $og_description ?></p>
      </div>
    </div>
  </div>
  <div class="article-other-info">
    <div class="article-keywords-found-panel">
      <h5>Keywords Found:</h5>
      <?php if(!empty($the_keywords)) { ?>
        <ul class="article-keywords-found-list">
            <?php foreach($the_keywords as $keyword): ?>
                <li><?= $keyword['keyword'] ?></li>
            <?php endforeach; ?>
        </ul>
        <?php } else { ?>
          <h5>No Keywords set for this.</h5> 
        <?php } ?>
    </div>
    <div class="article-fb-pages-to-share-panel">
    <?php
      if($article->article_type == 'published') {
        echo '<h5>Shared to:</h5>';
      } else {
        echo '<h5>Facebook Pages To Share:</h5>';
      }
    ?>
      <?php if(!empty($fbpage_info)) { ?>
        <ol class="article-facebook-page-list">
            <?php foreach($fbpage_info as $fb_page): ?>
              <li>
                <a href="<?= $fb_page['page_url'] ?>" target="_blank" >
                  <?= "(".$fb_page['fb_page_group'].")" ?>
                  <?= $fb_page['page_title'] ?>
                </a>
                <?php
                  
                  foreach($articleprocessqueues as $articleprocessqueue) {
                    if($articleprocessqueue->article_id == $article->id && $articleprocessqueue->fbpage_id == $fb_page['id']) {
                      if($articleprocessqueue->shared_status == 1) {
                        echo $this->cell('SharedCheck', [$article->id, $fb_page['id']]);
                      } else {
                        if($this->cell('SchedCheck', [$fb_page['id']]) == 'true') {
                          echo '<span class="ok-for-posting">OK to post</span>';
                        }
                      }
                    }
                  }
                  
                  ?>
                <?php
                  // if($article->article_type == 'published') {
                  //   foreach($articles_shared as $article_shared) {
                  //     if($article_shared->fbpage_id == $fb_page['id']) {
                  //       echo '<span class="pubished-graph-node-id">('.$article_shared->graph_node_id.')</span>';
                  //       echo ' - shared on '. $article_shared->created->i18nFormat('d/M/YYYY') . ' at '.$article_shared->created->i18nFormat('KK:mm:ss a');
                  //     }
                  //   }
                  // }
                ?>
              </li>
            <?php endforeach; ?>
        </ol>
        <?php } else { ?>
          <h5>This doesn't have any Facebook Page to share.</h5>
        <?php } ?>
    </div>
  </div>
  <div class="post-to-fb-container">
    <!-- <button class="post-to-fb">Post to Facebook</button> -->
    <?php
      if($article->article_type == 'raw') {
        // echo $this->Form->postLink(
        //   'Override: Manually Initiate Keyword Searching',
        //   ['action' => 'crawlArticle', $article->id],
        //   ['class' => 'post-to-fb override-button']
        // );

        $parse = parse_url($article->url);
        if($parse['host'] == 'www.showbizzsite.be') {
          echo $this->Form->postLink(
            'Override: Manually Initiate Keyword Searching',
            ['action' => 'crawlArticle', $article->id],
            ['class' => 'post-to-fb override-button']
          );
        }

        if($parse['host'] == 'www.vipnieuws.be' || $parse['host'] == 'entertainment-today.be') {
          echo $this->Form->postLink(
            'Override: Manually Initiate Keyword Searching',
            ['action' => 'wordpressCrawler', $article->id],
            ['class' => 'post-to-fb override-button']
          );
        }

        if($parse['host'] == 'www.msn.com') {
          echo $this->Form->postLink(
            'Override: Manually Initiate Keyword Searching',
            ['action' => 'msnCrawler', $article->id],
            ['class' => 'post-to-fb override-button']
          );
        }

        if($parse['host'] == 'sport247.be') {
          echo $this->Form->postLink(
            'Override: Manually Initiate Keyword Searching',
            ['action' => 'sport247Crawler', $article->id],
            ['class' => 'post-to-fb override-button']
          );
        }

        if($parse['host'] == 'www.showbizzsite.nl') {
          echo $this->Form->postLink(
            'Override: Manually Initiate Keyword Searching',
            ['action' => 'showbizzsitenlCrawler', $article->id],
            ['class' => 'post-to-fb override-button']
          );
        }
      } else if($article->article_type == 'processed') {
        // echo $this->Form->postLink(
        //   'Post This Article to Facebook',
        //   ['action' => 'postToFacebook', $article->id],
        //   ['class' => 'post-to-fb red-button']
        // );
      } else if($article->article_type == 'published') {
        echo '<h3 class="center">This Article has been shared to the Facebook Pages.</h3>';
      } else {
        echo '<h3 class="center">Update this Article to share to appropriate Facebook Pages.</h3>';
      }
    ?>
  </div>
  
</div>




