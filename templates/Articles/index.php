<h1>Queued Articles</h1>
  <div class="action-btn-container">
    <?= $this->Html->link('Add Articles', ['action' => 'add'],['class '=> 'button']) ?>
    <div class="cron-buttons">
      <?php 
          if($cron_status == true) {
            echo $this->Form->postLink(
              $this->Html->tag('i', '', array('class' => 'fas fa-toggle-on')) . " Pause Facebook Auto Post CRON",
              ['controller' => 'Articles', 'action' => 'toggleAutoPostCron', '0'],
              ['escape' => false]
            );
          } else {
            echo $this->Form->postLink(
              $this->Html->tag('i', '', array('class' => 'fas fa-toggle-off')) . " Initiate Facebook Auto Post CRON",
              ['controller' => 'Articles', 'action' => 'toggleAutoPostCron', '1'],
              ['escape' => false]
            );
          }
      ?>
    </div>
  </div>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>

<table>
  <tr>
    <th>No.</th>
    <th>Article Title</th>
    <th>Actions</th>
    <th>Article Published</th>
    <th>Sharing To Facebook Group/Page</th>
  </tr>
  <tbody>
    <?php
      $count = 1;
      foreach ($articles as $article): 
      $articleprocessqueues_ob = $article->articleprocessqueues;
    ?>
    <tr class="article-item">
      <td><?= $count++ ?></td>
      <td>
        <?php echo $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
      </td>
      <td>
        <?php echo $this->Html->link('Edit', ['action' => 'edit', $article->id], ['class' => 'red-button']) ?>
      </td>
      <td>
        <?= $article->lastmod ? $article->lastmod->i18nFormat('d/M/YYYY - hh:mm:ss a') : '' ?>
      </td>
      <td>
        <table>
          <?php 
            foreach($article->articleprocessqueues as $articleprocessqueue) {
              if($article->id == $articleprocessqueue->article_id && $articleprocessqueue->shared_status == 0) {
                foreach($fbpages as $fbpage) {
                  if($articleprocessqueue->fbpage_id == $fbpage->id) {
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href='.$fbpage->page_url.' class="fb-page-link" target="_blank"> ('. $fbpage->fb_page_group . ')</a> '. $this->Html->link(__($fbpage->page_title), ['controller' => 'Fbpages', 'action' => 'view', $fbpage->id]);

                    if($article->article_type == 'processed') {
                      if($this->cell('SchedCheck', [$fbpage->id]) == 'true') {
                        echo '<span class="ok-for-posting">OK to post</span>';
                      }
                    }

                    if($articleprocessqueue->error_message != NULL) {
                      echo '<span title="'.$articleprocessqueue->error_message.'" class="fb-share-failed">FB Share Failed</span>';
                    }
                    echo '</td>';
                    echo '<td>';
                    
                      if($article->article_type == 'processed') {
                        
                        echo $this->Html->link(
                                                'Share to Facebook',
                                                ['action' => 'postToFacebook', $articleprocessqueue->article_id, $articleprocessqueue->fbpage_id, $articleprocessqueue->id],
                                                ['class' => 'override-button']
                                              ); 
                      }

                    echo '</td>';
                    echo '</tr>';
                  }
                }
              }
            }
          ?>
        </table>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<div class="paginator">
  <ul class="pagination">
      <?= $this->Paginator->first('<< ' . __('first')) ?>
      <?= $this->Paginator->prev('< ' . __('previous')) ?>
      <?= $this->Paginator->numbers() ?>
      <?= $this->Paginator->next(__('next') . ' >') ?>
      <?= $this->Paginator->last(__('last') . ' >>') ?>
  </ul>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
</div>

<div style="text-align:center; display:none;">
    <?= $this->Form->postLink(
                    'For Testing Purposes Only: Delete All Articles',
                    ['action' => 'clearArticles'],
                    ['class '=> 'red-button'])
    ?>
</div>



