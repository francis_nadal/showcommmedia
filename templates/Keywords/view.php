<div class="keyword-view">
  <div class="edit-section">
    <?= $this->Html->link(
                          'Edit',
                          ['action' => 'edit', $keyword->id],
                          ['class' => 'red-button']
                        ) ?>
  </div>

  <h1><?= h($keyword->keyword) ?></h1>

  <h5>Assigned to these Facebook Pages:</h5>

  <?php if(!empty($fb_info)) { ?>
    <ul class="article-facebook-page-list">
      <?php foreach($fb_info as $fb_page): ?>
        <!-- <li><?php// echo $fb_page['page_title']; ?></li> -->
        <li><a href="<?= $fb_page['page_url'] ?>" target="_blank" ><?= $fb_page['page_title'] ?></a></li>
      <?php endforeach; ?>
    </ul>
  <?php } else { ?>
    <h5>This keyword is not assigned to any Facebook Page.</h5>
  <?php } ?>
</div>