<h1>Edit Keyword</h1>

<?php

  echo $this->Form->create($keyword);
  echo $this->Form->control('id', ['type' => 'hidden']);
  echo $this->Form->control('keyword');
  echo '<label class="article-fb-caption">Assign keyword to Facebook Pages:</label>';
  echo '<div class="article-fb-selection">';
  echo '<ul class="fbpages-column">';
  
  foreach($fbpages as $index => $fbpage) :
    if($fbpage['fb_page_group'] == 'Group') {
      $display = 'style="display: none;"';
    } else {
      $display = '';
    }

    $checked = false;
    
    foreach($keyword->fbpagekeywords as $keyword_item) {
      if($keyword_item['fbpage_id'] === $fbpage['id']) {
        $checked = true;
      } 
    }
  
    echo '<li '.$display.'><label for="showcomm_checkbox_'.$index.'" class="article-fb-option">' 
      . $this->Form->checkbox( 'fbpagekeywords.'.$index.'.fbpage_id',
        ['value' => $fbpage['id'],
        'id' => 'showcomm_checkbox_'.$index.'',
        'hiddenField' => false,
        'checked' => $checked
        ]) 
      . $fbpage['page_title'] . ' (' . $fbpage['fb_page_group'] . ')</label></li>';
  endforeach;
  
  echo '</ul>';
  echo '</div>';
  echo $this->Form->button(__('Update'));
  echo $this->Form->end();

?>