<h1>Add Keyword</h1>

<?php

  echo $this->Form->create($keyword);
  echo $this->Form->control('keyword');
  echo '<label class="article-fb-caption">Assign keyword to Facebook Pages:</label>';
  echo '<div class="article-fb-selection">';
  echo '<ul class="fbpages-column">';
  foreach($fbpages as $index => $fbpage) :
    if($fbpage['fb_page_group'] == 'Page') {
      echo '<li><label for="showcomm_checkbox_'.$index.'" class="article-fb-option">' . $this->Form->checkbox('fbpagekeywords.'.$index.'.fbpage_id', ['value' => $fbpage['id'], 'id' => 'showcomm_checkbox_'.$index.'', 'hiddenField' => false]) . $fbpage['page_title'] . ' (' . $fbpage['fb_page_group'] . ')</label></li>';
    }
  endforeach;
  echo '</ul>';
  echo '</div>';
  echo '<span class="create-btn">' . $this->Form->button(__('Add')) . '</span>';
  echo $this->Form->end();

?>