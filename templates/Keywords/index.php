<h1>Keywords</h1>
  <?= $this->Html->link('Add Keywords', ['action' => 'add'],['class '=> 'button']) ?>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
<table class="index">
    <tr>
        <th>Keyword</th>
        <th style="text-align:center;">Assigned Facebook Pages</th>
        <th>Actions</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($keywords as $keyword): ?>
    <tr>
        <td>
          <?= $this->Html->link($keyword->keyword, ['action' => 'view', $keyword->id]) ?>
        </td>
        <td style="text-align:center;">
          <?php
            if(isset($keyword->fbpagekeywords)) {
              // foreach($keyword->fbpagekeywords as $fbpagekeyword) {
                echo count($keyword->fbpagekeywords);
              // }
            }
          ?>
        </td>
        <td>
            <?= $this->Html->link('Edit', ['action' => 'edit', $keyword->id], ['class' => 'red-button']) ?>
            <?= $this->Form->postLink(
                    'Delete',
                    ['action' => 'delete', $keyword->id],
                    ['confirm' => 'Are you sure?'])
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<div class="paginator">
  <ul class="pagination">
    <?= $this->Paginator->first('<< ' . __('first')) ?>
    <?= $this->Paginator->prev('< ' . __('previous')) ?>
    <?= $this->Paginator->numbers() ?>
    <?= $this->Paginator->next(__('next') . ' >') ?>
    <?= $this->Paginator->last(__('last') . ' >>') ?>
  </ul>
  <div class="table-info">
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
</div>