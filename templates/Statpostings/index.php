<h1>Stats Postings</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($statpostings as $statposting): ?>
    <tr>
        <td>
          <?= $statposting->id; ?>
        </td>
        <td>
          <?= $this->Html->link($statposting->name, ['action' => 'view', $statposting->id]) ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>