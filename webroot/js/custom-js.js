// A $( document ).ready() block.
;(function($, window, document, undefined) {
  $( document ).ready(function() {
    console.log( "ready!" );
    addTimeRows();    
    hideTokenField();
    generateToken();
    unSelectAll();
    selectAllKeywords();
    showInfo();
    clearError();
    
    function addTimeRows() {
      var btnAdd = $('#btn-add-time');
        // btnRemove = $("a[id^='btn-remove-time-']").attr('id');
        btnWrap = $('#fbpage-form').find('.sched-time-fields');
        template = $.trim($('#template-inputs').html());

        btnAdd.on('click', function(e) {
          countItems = $('.sched-time-fields .sched-time-row').has('input').length ? "yes" : "no";
          if(countItems == "no") {
            $('.sched-time-fields .sched-time-row:first-child').remove();
            nextItem = 0; 
          } else {
            nextItemId = $('.sched-time-fields .sched-time-row:last-child input').attr('id').replace(/sched-wrap-/, '');
            nextItem = parseInt(nextItemId) + 1;
          }
          e.preventDefault();
          // console.log("add click!");
          addNewInputToTheForm(nextItem);
        });

        function addNewInputToTheForm(item) {
          var newItemHtml = template.replace(/::num/g, item );
          btnWrap.append(newItemHtml);
          // console.log(newItemHtml);
        }

        $("#fbpage-form").delegate('a.remove-me','click', function(e) {
          e.preventDefault();
          var id = $(this).parent();
          id.remove();
      });
    }

    function generateToken() {
      var btnGenerate = $('#btn-generate-token');

      btnGenerate.on('click', function(e) {
        e.preventDefault();
        console.log("Generate Token");
        if($('#pageid').val() != '') {
          $.ajax({
            type:'post',
            url: '/fbpages/generateToken/' + $('#pageid').val(),
            dataType: 'json',
            beforeSend: function() {
              $('#btn-generate-token').html('Please wait ...');
              $('#btn-generate-token').attr('disabled','disabled');
            },
            success:function(result){
              console.log(result);
              if(result.error != null) {
                alert(result.error.message);
              } else {
                $('#fb_access_token').val(result.access_token);
              }
              $('#btn-generate-token').html('Generate Page Token');
              $('#btn-generate-token').removeAttr('disabled');
            }
          });
        } else {
          alert('Facebook Group/Page ID field is required to generate Token.');
        }
      });
    }

    function hideTokenField() {
      var btnPageGroup = $('input[name="fb_page_group"]');

      btnPageGroup.on('change', function(e) {
        e.preventDefault();
        if($('input[name="fb_page_group"]:checked').val() == 'Group') {
          $('#fb_access_token').val('');
          $('#fb_access_token').hide();
          $('#fb_access_token_label').hide();
          $('#btn-generate-token').hide();
          $('.more-options').hide();          
        } else {
          $('#fb_access_token').show();
          $('#fb_access_token_label').show();
          $('#btn-generate-token').show();
          $('.more-options').show();    
        }
      });
    }

    function unSelectAll() {
      $('#unselect-all').click(function(e){
        e.preventDefault();
        $( "ul.fbpages-column input[type=hidden]" ).each(function( index ) {
          if($(this).val() != 1) {
            $('ul.fbpages-column input#showcomm_checkbox_'+index).prop('checked','');
          }
        });
      });
    }

    function showInfo() {
      $('span#shared-icon-info').hover(function() {
        $(this).find('span#shared-tooltip').css('display','inline-block');
      }, function() {
        $(this).find('span#shared-tooltip').css('display','none');
      });

      $('span#error-icon-info').hover(function() {
        $(this).find('span#error-tooltip').css('display','inline-block');
      }, function() {
        $(this).find('span#error-tooltip').css('display','none');
      });
    }

    function clearError() {

      $( ".article-edit-form .article-fb-option" ).each(function( index ) {
        $('.article-edit-form label#fb_selection_'+index).click(function(e) {
          
          // let error_message = $(this).find('#error_message_'+index).val();
          
          $(this).find('#error_message_'+index).removeAttr('value');
          $(this).find('#error_message_'+index).attr('disabled','disabled');
        })
        
      })
      

      // $('.article-edit-form .article-fb-option label , .article-edit-form .article-fb-option input' ).click(function(e) {
      //   console.log("checkbox clicked");
      //   let error_message = $(this).find('.error-message-textfield').val();
      //   console.log(error_message);
      //   // if($(this).val() != 1) {
      //   //   $('ul.fbpages-column input#showcomm_checkbox_'+index).prop('checked','');
      //   // }
      // });
    }

    function selectAllKeywords() {
      var allSelected = $('.keyword-checkbox:checked').length === $('.keyword-checkbox').length;

      $('#selectAllKeywords').text(allSelected ? 'Unselect All' : 'Select All');

      $('#selectAllKeywords').click(function(e) {
          e.preventDefault();
          if (!allSelected) {
              $('.keyword-checkbox').prop('checked', true);
              $(this).text('Unselect All');
          } else {
              $('.keyword-checkbox').prop('checked', false);
              $(this).text('Select All');
          }
          allSelected = !allSelected;
      });
    }
  });
})(jQuery, window, document);